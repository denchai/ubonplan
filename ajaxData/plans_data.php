<?php
// Start the session
session_start();
//$_SESSION["vaccation_year"] = "59";
header('Content-Type: text/html; charset=utf-8');
function unset_n_reset(&$arr, $key)
{
    unset($arr[$key]);
    $arr = array_merge($arr);
}
include "../connect.php";
$user_id = $_SESSION["user_id"];
// mysql_query("set character_set_results=utf8");
// mysql_query("set character_set_connection=utf8");
// mysql_query("set character_set_client=utf8");

//$vn = $_GET['vn'];
//$vn = 863114;

$data = array();

if (isset($_GET['plan_id'])) {
    $plan_id = $_GET['plan_id'];
    //$plan_id = 1;
    $sql = "select a.*,s.strategy_fullname,p.plan_fullname,pr.project_name,b.budget_name,sp.status_name,u.fullname from activity_plan_list a
        join strategy_items s on s.strategy_id = a.strategy_id
        join plan_items p on p.plan_id=a.plan_id
        join project_items pr on pr.project_id = a.project_id
        join budget b on b.budget_id=a.budget_id
        join status_plan sp on sp.status_id=a.status_id
        join users u on u.user_id=a.user_id
        where a.activity_plan_id=$plan_id ";

    if ($result = mysqli_query($con, $sql)) {
        while ($row = mysqli_fetch_array($result, MYSQLI_ASSOC)) {
            $row_array['activity_plan_id'] = $row['activity_plan_id'];
            $row_array['strategy_id'] = $row['strategy_id'];
            $row_array['strategy_fullname'] = $row['strategy_fullname'];
            $row_array['plan_id'] = $row['plan_id'];
            $row_array['plan_fullname'] = $row['plan_fullname'];
            $row_array['plan_name'] = $row['plan_name'];
            $row_array['project_id'] = $row['project_id'];
            $row_array['project_name'] = $row['project_name'];
            $row_array['budget_id'] = $row['budget_id'];
            $row_array['budget_name'] = $row['budget_name'];
            $row_array['date_input'] = $row['date_input'];
            $row_array['date_update'] = $row['date_update'];
            $row_array['owner'] = $row['owners'];
            $row_array['budget_name'] = $row['budget_name'];
            $row_array['status_name'] = $row['status_name'];
            $row_array['purpose'] = $row['purpose'];
            $row_array['budget_request'] = $row['budget_request'];
            $row_array['user_id'] = $row['user_id'];
            $row_array['sub_activity'] = array();

            array_push($data, $row_array);
        }

        // echo json_encode($data);
    }
    $sql_sub_activity = "select * from sub_activity_plan_list where activity_plan_id=$plan_id";
    if ($result_sub = mysqli_query($con, $sql_sub_activity)) {
        while ($row = mysqli_fetch_array($result_sub, MYSQLI_ASSOC)) {
            $row_array_sub['sub_activity_id'] = $row['sub_activity_id'];
            $row_array_sub['activity_plan_id'] = $row['activity_plan_id'];
            $row_array_sub['sub_activity_name'] = $row['sub_activity_name'];
            $row_array_sub['month_activity'] = $row['month_activity'];
            $row_array_sub['sub_activity_target'] = $row['sub_activity_target'];
            $row_array_sub['sub_activity_purpose'] = $row['sub_activity_purpose'];

            array_push($data[0]['sub_activity'], $row_array_sub);
        }
    }
    echo json_encode($data);
} else {
    if ($_SESSION["user_level"] == '3') {
        $sql = "select p.* ,b.budget_name,s.status_name from activity_plan_list p
        join budget b on b.budget_id=p.budget_id
        join status_plan s on s.status_id=p.status_id
       ";

    } else {
        $sql = "select p.* ,b.budget_name,s.status_name from activity_plan_list p
    join budget b on b.budget_id=p.budget_id
    join status_plan s on s.status_id=p.status_id
    where user_id='$user_id' ";
    }

    if ($result = mysqli_query($con, $sql)) {
        while ($row = mysqli_fetch_array($result, MYSQLI_ASSOC)) {
            $row_array['activity_plan_id'] = $row['activity_plan_id'];
            $row_array['strategy_id'] = $row['strategy_id'];
            $row_array['plan_name'] = $row['plan_name'];
            $row_array['date_input'] = $row['date_input'];
            $row_array['date_update'] = $row['date_update'];
            $row_array['owner'] = $row['owners'];
            $row_array['budget_name'] = $row['budget_name'];
            $row_array['budget_request'] = $row['budget_request'];
            $row_array['status_name'] = $row['status_name'];
            $row_array['user_id'] = $row['user_id'];

            array_push($data, $row_array);
        }

        $myObj->data = $data;
        echo json_encode($myObj);
    }
}

exit;
