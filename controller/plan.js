var subActivityArray = new Array(new Array());
var subActivityLen = 0;

$(document).ready(function() {

    $('#editPlanBasic').addClass('d-none');
    $('#editPlanImage').addClass('d-none');

    $("#plan_image").change(function() {
        readURLimg(this);
    });


});
// $('input[name="strategy"]').on('click', function(e) {
//     get_plan(this.value);
// });


$("button#strategy_radio").on('change', function(e) {
    //$(this).removeClass("btn-outline-secondary");
    //$(this).addClass("btn-primary");
    var vall = $(this).find("input").attr('value');
    $(this).find("input").prop('checked', true);
    get_plan(vall);
});

//create alert  dialog  myAlert name global
alertify.myAlert || alertify.dialog('myAlert', function factory() {
    return {
        main: function(content) {
            this.setContent(content);
        },
        setup: function() {
            return {
                options: {
                    modal: false,
                    basic: true,
                    maximizable: true,
                    resizable: false,
                    padding: false,
                    transition: 'fade',
                    autoReset: false

                }
            };
        }
    };
});

function popupEditPlanBasic(plan_id) {
    $("input#activity_plan_id").val(plan_id);
    $('#editPlanBasic').removeClass('d-none');
    alertify.closeAll();

    get_plan_edit(plan_id);

    alertify.genericDialog1327195 || alertify.dialog('genericDialog1327195', function() {
        return {
            main: function(content) {
                this.setContent(content);
            },
            setup: function() {
                return {
                    focus: {
                        element: function() {
                            return this.elements.body.querySelector(this.get('selector'));
                        },
                        select: true
                    },
                    options: {
                        basic: true,
                        maximizable: true,
                        closableByDimmer: true,
                        resizable: false,
                        transition: 'fade',
                        autoReset: false,
                        padding: false

                    },
                    glossary: {
                        title: 'คำเตือน',
                        ok: 'OK',
                        cancel: 'Cancel'
                    },
                    theme: {
                        input: 'ajs-input',
                        ok: 'ajs-ok',
                        cancel: 'ajs-cancel'
                    }
                };
            },
            prepare: function() {
                this.setContent(this.message);
                this.elements.footer.style.visibility = "hidden";
            },
            hooks: {
                onshow: function() {
                    this.elements.dialog.style.maxWidth = 'none';
                    this.elements.dialog.style.width = '80%';
                },
                onclose: function() {
                    alertify.dialog.closeAll;

                }
            },


            settings: {
                selector: undefined
            }
        };
    });

    //dialogshow(id);
    //force focusing password box
    $('div#sub_activity').not(':first').remove();
    alertify.genericDialog1327195($('div#editPlanBasic')[0]).set('selector', 'input[type="text"]');

}

function popupEditPlanImage(plan_id) {
    $("input#activity_plan_id").val(plan_id);
    $("input#plan_id_img").val(plan_id);
    $('#editPlanImage').removeClass('d-none');
    //plan_id_img
    alertify.closeAll();

    displayImage(plan_id);
    console.log('me');



    alertify.genericDialog1327195 || alertify.dialog('genericDialog1327195', function() {
        return {
            main: function(content) {
                this.setContent(content);
            },
            setup: function() {
                return {
                    focus: {
                        element: function() {
                            return this.elements.body.querySelector(this.get('selector'));
                        },
                        select: true
                    },
                    options: {
                        basic: true,
                        maximizable: true,
                        closableByDimmer: true,
                        resizable: false,
                        transition: 'fade',
                        autoReset: false,
                        padding: false

                    },
                    glossary: {
                        title: 'คำเตือน',
                        ok: 'OK',
                        cancel: 'Cancel'
                    },
                    theme: {
                        input: 'ajs-input',
                        ok: 'ajs-ok',
                        cancel: 'ajs-cancel'
                    }
                };
            },
            prepare: function() {
                this.setContent(this.message);
                this.elements.footer.style.visibility = "hidden";
            },
            hooks: {
                onshow: function() {
                    this.elements.dialog.style.maxWidth = 'none';
                    this.elements.dialog.style.width = '80%';
                },
                onclose: function() {
                    alertify.dialog.closeAll;

                }
            },


            settings: {
                selector: undefined
            }
        };
    });

    //dialogshow(id);
    //force focusing password box
    alertify.genericDialog1327195($('div#editPlanImage')[0]).set('selector', 'input[type="text"]');

}

function get_strategy(val) {

    $('select#select_plan').empty();
    $('select#select_plan').append('<option value="" selected>Choose...</option>');
    $.getJSON('model/get_plan.php', { 'data_type': 'plan', 'id': val }, function(data) {

        $.each(data, function(key, value) {

            $('select#select_plan').append("<option value='" + value.plan_id + "'>" + value.plan_name + "</option>");
        });
    });
};

function get_plan(val) {

    $('select#select_plan').empty();
    $('select#select_plan').append('<option value="" selected>Choose...</option>');
    $.getJSON('model/get_plan.php', { 'data_type': 'plan', 'id': val }, function(data) {

        $.each(data, function(key, value) {

            $('select#select_plan').append("<option value='" + value.plan_id + "'>" + value.plan_name + "</option>");
        });
    });
};

function get_project(e) {
    var plan_id = e.value;
    //alert(plan_id);
    $('select#select_project').empty();
    $('select#select_project').append('<option value="" selected>Choose...</option>');
    $.getJSON('model/get_plan.php', { 'data_type': 'project', 'id': plan_id }, function(data) {

        $.each(data, function(key, value) {
            $('select#select_project').append("<option value='" + value.project_id + "'>" + value.project_name + "</option>");
        });

    });
};

function get_plan_edit(plan_id) {

    console.log(plan_id);

    $.getJSON('ajaxData/plans_data.php', { 'plan_id': plan_id }, function(data) {

        $.each(data, function(key, value) {
            console.log(data);

            $("input#strategy" + value.strategy_id).prop('checked', true);
            get_plan(value.strategy_id);

            setTimeout(function() {
                $("select#select_plan").val(value.plan_id).change();
            }, 500);
            setTimeout(function() {
                $("select#select_project").val(value.project_id).change();
            }, 1000);
            setTimeout(function() {
                $("input#activity_plan").val(value.plan_name);
                $("input#budget" + value.budget_id).prop('checked', true);
                $("input#budget_request").val(value.budget_request);
                $("input#owner").val(value.owner);
                // alert(value.purpose);
                $("textarea#purpose").val(value.purpose);
            }, 1500);
            //var len_sub_activity = value.sub_activity.length;
            console.log(value.sub_activity);
            addActivity_sub(value.sub_activity);
        });

    });
}

function get_plan_edit_image(plan_id) {

    // alert(plan_id);

    $.getJSON('ajaxData/plans_data.php', { 'plan_id': plan_id }, function(data) {
        $.each(data, function(key, value) {
            console.log(data);


        });
    });
}

function add_planBasicold() {
    //console.log('me');
    var activity_plan_id = $("input#activity_plan_id").val();

    var form = $('form#add_plan_basic');

    //alert(form.serialize());
    //var strategy = $('input[name=strategy]:checked').val();

    var checkStrategy = $("input[name=strategy]").is(":checked"); //return false or true
    //var strategyVal = $("input[name=strategy]").val();
    var plan = $("select#select_plan option:selected").val(); //unselect return null value
    var project = $("select#select_project option:selected").val();
    var activity_plan = $("input#activity_plan").val();
    var budget = $("input[name=budget]").is(":checked");
    var purpose = $("input#purpose").val();
    var owner = $("input#owner").val();
    //alert(JSON.stringify(form));
    if (checkStrategy == false || plan == "" || project == "" || activity_plan == "" || budget == false || owner == "" || purpose == "") {
        // alertify.error('คำแนะนำ', 'ICD10 ไม่ถูกต้อง', function() {
        //     alertify.success('!Error');
        // });
        alertify.alert("คำเตื่อน กรอกข้อมูลไม่ครบ.", function() {
            //alertify.message('OK');
        });

    } else {
        //alert('me');
        $.ajax({

            url: 'ajaxData/add_plansBasic.php?activity_plan_id=' + activity_plan_id,
            type: "POST",
            data: form.serialize(),
            dataType: "html",
            // headers: {
            //     'Cache-Control': 'no-cache, no-store, must-revalidate',
            //     'Pragma': 'no-cache',
            //     'Expires': '0'
            // },
            // cache: false
            success: function(data) {
                // alert(data);
                var obj = JSON.parse('{"AlertList" : ' + data + '}');

                if (obj.AlertList[0].result == "Save Success") {
                    alertify.alert('คำแนะนำ', obj.AlertList[0].result, function() {
                        //alertify.message('OK');
                    });
                    $("button#save_plan_basic").prop("disabled", true);
                    $("button#first_step").removeClass('btn-default');
                    $("button#first_step").addClass('btn-primary');

                    activity_plan_id = obj.AlertList[0].return_id;

                    $('div#step2,div#step3').show();
                    $('input#plan_id').val(activity_plan_id);
                    alertify.closeAll();

                } else {
                    alertify.alert('คำเตือน', obj.AlertList[0].result, function() {
                        //alertify.message('OK');
                    });
                }

            },
            error: function(data) {
                alertify.alert("เกิดข้อผิดพลาด ไม่สามารถบันทึกได้", function() {
                    //alertify.message('OK');
                });

            },
        })

    }
}

function displayImage(plan_id) {
    console.log(plan_id);
    $('div#image_sms').empty();
    $.getJSON('ajaxData/get_image.php', { 'plan_id': plan_id }, function(data) {
        //console.log(data[0]);
        if (data) {
            $.each(data, function(key, value) {
                //console.log(value.blob_image);
                var image = $("<img>", {
                    "src": "data:image/jpeg;base64," + value.blob_image,
                    "width": "250px",
                    "height": "250px"
                });
                $("p#old_img_txt").text('ภาพเดิม');
                $('div#image_sms').html(image);
                //console.log('me2');
                // '<img src="data:image/jpeg;base64,'.base64_encode( $result['img'] ).'"/>';
            });
        } else {
            // $('div#image_sms').html(image);
            alert('me');
            $("div#image_sms").attr("src", "images/No_sign.png");
            $("p#old_img_txt").text('ยังไม่เคยอัพโหลดภาพ');
        }
    });
}

function readURLimg(input) {
    $('#img_preview').attr('src', '#');
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function(e) {
            $('#img_preview').attr('src', e.target.result);
        }

        reader.readAsDataURL(input.files[0]);
        $("p#new_img_txt").text('ภาพใหม่ที่จะอัพโหลด');

    }
}

function saveImageSms() {
    var plan_id = $("input#activity_plan_id").val();
    console.log(plan_id);
    $("input#plan_id").val(plan_id);
    var form = $('form#image_form')[0];

    var image_name = $('#plan_image').val();

    if (image_name == '') {
        alert("Please Select Image");
        return false;
    } else {

        var extension = $('#plan_image').val().split('.').pop().toLowerCase();
        if (jQuery.inArray(extension, ['gif', 'png', 'jpg', 'jpeg']) == -1) {

            alert("Invalid Image File");
            $('#plan_image').val('');
            return false;
        } else {
            //alert('me');
            //console.log('me');
            $.ajax({
                url: "ajaxData/add_image.php?plan_id=",
                method: "POST",
                data: new FormData(form),
                contentType: false,
                processData: false,
                success: function(data) {
                    var obj = JSON.parse('{"AlertList" : ' + data + '}');
                    if (obj.AlertList[0].result == "Save Success") {
                        alertify.alert('คำแนะนำ', obj.AlertList[0].result, function() {
                            //alertify.message('OK');
                            $('#image_form')[0].reset();

                            alertify.closeAll();
                        });


                    } else {
                        alertify.alert('คำเตือน', obj.AlertList[0].result, function() {
                            //alertify.message('OK');
                        });
                    }




                }
            });
        }
    }
}

function delPlan(plan_id) {
    // console.log(plan_id)

    alertify.confirm('คำเตือน', 'คุณต้องการจะลบรายการนี้หรือไม่', function() {

        $.getJSON('model/plan_list.php', { 'type': 'del', 'plan_id': plan_id }, function(data) {
            //var obj = JSON.parse('{"AlertList" : ' + data + '}');

            $.each(data, function(key, value) {
                console.log(value.result);
                alertify.success('Ok Delete Plan Id: ' + value.return_id);

            });

        });


    }, function() {
        alertify.error('Cancel')
    });

}



function addActivity_sub(sub_activity) {
    // for iterate data to element
    //clone element  on  edit
    //console.log(sub_activity[0]);

    var len_sub_activity = sub_activity.length;
    //declate to global  for  medthod  addActivity
    subActivityLen = len_sub_activity;
    console.log('subActivityLen' + subActivityLen);

    $('#sub_activity_id1').val(sub_activity[0].sub_activity_id);
    $('#sub_activity1').val(sub_activity[0].sub_activity_name);
    $('#activity_month1').val(sub_activity[0].month_activity).change();
    $('#sub_activity_target1').val(sub_activity[0].sub_activity_target);
    $('#sub_activity_purpose1').val(sub_activity[0].sub_activity_purpose);


    for (var i = 1; i < len_sub_activity; i++) {
        var j = i + 1;

        //console.log(sub_activity[i]);
        //console.log(j);
        var clone = $("div#sub_activity:first").clone();
        clone.find("label").remove();
        clone.find("div#btn_add").remove();

        //hidden input stord id
        clone.find('#sub_activity_id1').attr('value', '');
        clone.find('#sub_activity_id1').attr('id', 'sub_activity_id' + j);
        clone.find('#sub_activity_id1').attr('name', 'sub_activity_id' + j);

        // input stord value name
        clone.find('#sub_activity1').attr('value', '');
        clone.find('#sub_activity1').attr('id', 'sub_activity' + j);
        clone.find('#sub_activity1').attr('name', 'sub_activity' + j);

        // input stord value target
        clone.find('#sub_activity_target1').attr('value', '');
        clone.find('#sub_activity_target1').attr('id', 'sub_activity_target' + j);
        clone.find('#sub_activity_target1').attr('name', 'sub_activity_target' + j);

        // input stord value purpose
        clone.find('#sub_activity_purpose1').attr('value', '');
        clone.find('#sub_activity_purpose1').attr('id', 'sub_activity_purpose' + j);
        clone.find('#sub_activity_purpose1').attr('name', 'sub_activity_purpose' + j);



        //store month
        clone.find('#activity_month1').attr('id', 'activity_month' + j);
        clone.find('#activity_month1').attr('name', 'activity_month' + j);

        clone.find('#sub_activity_id' + j).val(sub_activity[i].sub_activity_id);
        clone.find('#sub_activity' + j).val(sub_activity[i].sub_activity_name);
        clone.find('#activity_month' + j).val(sub_activity[i].month_activity).change();
        clone.find('#sub_activity_target' + j).val(sub_activity[i].sub_activity_target);
        clone.find('#sub_activity_purpose' + j).val(sub_activity[i].sub_activity_purpose);

        clone.insertAfter("div#sub_activity:last");


    }


}

function addActivityPlan() {
    //on click button  to append element
    var countAdd = ($("div#sub_activity").length) + 1;
    //console.log(countAdd);

    var clone = $("div#sub_activity:first").clone();
    clone.find("label").remove();
    clone.find("div#btn_add").remove();

    clone.find('#sub_activity_id1').val('');
    clone.find('#sub_activity_id1').attr('id', 'sub_activity_id' + countAdd);
    clone.find('#sub_activity_id1').attr('name', 'sub_activity_id' + countAdd);

    clone.find('#sub_activity1').attr('id', 'sub_activity' + countAdd);
    clone.find('#sub_activity1').attr('name', 'sub_activity' + countAdd);

    clone.find('#activity_month1').attr('id', 'activity_month' + countAdd);
    clone.find('#activity_month1').attr('name', 'activity_month' + countAdd);

    clone.insertAfter("div#sub_activity:last");


}

function add_planBasic() {

    var activity_plan_id = $("input#activity_plan_id").val();
    console.log('activity_plan_id:' + activity_plan_id);
    var arrData = {};
    var form = $('form#add_plan_basic');

    var checkStrategy = $("input[name=strategy]").is(":checked"); //return false or true

    var plan = $("select#select_plan option:selected").val(); //unselect return null value
    var project = $("select#select_project option:selected").val();
    var activity_plan = $("input#activity_plan").val();
    var budget = $("input[name=budget]").is(":checked");
    var purpose = $("input#purpose").val();
    var owner = $("input#owner").val();
    console.log($("input[name=budget]").val());
    //alert(JSON.stringify(form));
    if (checkStrategy == false || plan == "" || project == "" || activity_plan == "" || budget == false || owner == "" || purpose == "") {
        alertify.alert("คำเตื่อน กรอกข้อมูลไม่ครบ.", function() {
            //alertify.message('OK');
        });

    } else {
        $.ajax({

            url: 'ajaxData/add_plansBasic.php?activity_plan_id=' + activity_plan_id,
            type: "POST",
            // data: form.serialize() + "&variable=" + jsonString,
            data: form.serialize(),
            dataType: "html",
            success: function(data) {
                console.log(data);
                var obj = JSON.parse('{"AlertList" : ' + data + '}');
                console.log(obj.AlertList[0].return_id);
                if (obj.AlertList[0].result == "Save Success") {
                    //insert  sub
                    console.log('inser_sub befor');
                    addSubActivity(obj.AlertList[0].return_id);

                    $("button#save_plan_basic").prop("disabled", true);
                    $("button#first_step").removeClass('btn-default');
                    $("button#first_step").addClass('btn-primary');

                    activity_plan_id = obj.AlertList[0].return_id;
                    //console.log(activity_plan_id);
                    $("input#activity_plan_id").val(activity_plan_id);

                    $('div#step2,div#step3').show();
                    $('input#plan_id').val(activity_plan_id);
                    alertify.closeAll();


                } else {
                    alertify.alert('คำเตือน', obj.AlertList[0].result, function() {
                        //alertify.message('OK');
                    });
                }

            },
            error: function(data) {
                alertify.alert("เกิดข้อผิดพลาด ไม่สามารถบันทึกได้", function() {
                    //alertify.message('OK');
                });

            },
        })

    }
}

function addSubActivity(activity_plan_id) {
    subActivityArray[0] = [];
    console.log('plan');
    console.log(activity_plan_id);
    var len_sub_activity = $('input.sub_activity').length;

    var ac_id = $('input.sub_activity_id');
    var ac = $('input.sub_activity');
    var target = $('input.sub_target');
    var purpose = $('input.sub_purpose');
    var mo = $("select.sub_month option:selected");

    for (var i = 1; i <= len_sub_activity; i++) {
        var obj = {
            "subActId": ac_id.eq(i - 1).val(),
            "subAct": ac.eq(i - 1).val(),
            "subTar": target.eq(i - 1).val(),
            "subPur": purpose.eq(i - 1).val(),
            "monthAct": $('select#activity_month' + i + ' option:selected').val()
        }
        subActivityArray[0].push(obj);

    }
    console.log(subActivityArray);

    $.getJSON('model/add_sub_activity.php', {
        'data': subActivityArray[0],
        'plan_id': activity_plan_id
    }, function(data) {
        //var obj = JSON.parse('{"AlertList" : ' + data + '}');
        //console.log(data);
        $.each(data, function(key, value) {
            // alertify.alert('คำแนะนำ', data[0].result, function() {
            //     alertify.message('OK');
            //     window.location.href = "index.php";
            // });
            alertify.alert('คำแนะนำ', data[0].result, function(e) {
                if (e) {
                    window.location.href = "index.php";
                }
            });


        });
    });

}



function addActivity() {
    subActivityLen++;
    console.log('me plan');
    //recieve  global variable subActivityLen from method add sub


    //countAdd++;
    console.log('subActivityLen' + subActivityLen);
    var arrData = {};
    //console.log(i);

    var clone = $("div#sub_activity:first").clone();
    clone.find("label").remove();
    clone.find("div#btn_add").remove();
    //hidden id
    sub_activity_id1
    clone.find('#sub_activity_id1').val('');
    clone.find('#sub_activity_id1').attr('name', 'sub_activity_id' + subActivityLen);
    clone.find('#sub_activity_id1').attr('id', 'sub_activity_id' + subActivityLen);

    //activity name
    clone.find('#sub_activity1').val('');
    clone.find('#sub_activity1').attr('name', 'sub_activity' + subActivityLen);
    clone.find('#sub_activity1').attr('id', 'sub_activity' + subActivityLen);

    // button del
    clone.find('#delAct1').attr('id', 'delAct' + subActivityLen);
    // month
    clone.find('#activity_month1').attr('name', 'activity_month' + subActivityLen);
    clone.find('#activity_month1').attr('id', 'activity_month' + subActivityLen);
    //target
    clone.find('#sub_activity_target1').val('');
    clone.find('#sub_activity_target1').attr('name', 'sub_activity_target' + subActivityLen);
    clone.find('#sub_activity_target1').attr('id', 'sub_activity_target' + subActivityLen);
    //purpose
    clone.find('#sub_activity_purpose1').val('');
    clone.find('#sub_activity_purpose1').attr('name', 'sub_activity_purpose' + subActivityLen);
    clone.find('#sub_activity_purpose1').attr('id', 'sub_activity_purpose' + subActivityLen);


    clone.insertAfter("div#sub_activity:last");

}

function delActivity(e) {

    var last1 = e.substr(-1);
    //alert(last1);
    if (last1 > 1) {
        $('#sub_activity' + last1).parents('#sub_activity').remove();
    }

}