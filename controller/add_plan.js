var activity_plan_id = '';
var rateArray = new Array(new Array());
var subActivityArray = new Array(new Array());
var sumPrice = 0;
var plan_budget = Number($('input#plan_budget').val());



$(document).ready(function() {

    if ($('input#plan_id').length) // use this if you are using id to check
    {
        // show hide element
        // alert($('input#plan_id').length);
        $('#step2').show();

    } else {
        // alert('me');
        $('#step2').hide();
        $('#step3').hide();

    }
    var i;
    for (i = 0; i < 13; i++) {
        $('table#cost tbody tr:eq(' + i + ')').hide();
    }
    var plan_id = $('input#plan_id').val();
    get_sub_activity(plan_id);

});

// $('input[name="strategy"]').on('click change', function(e) {

// $('input[name="strategy"]').on('click', function(e) {
//     get_plan(this.value);
// });


$("button#strategy_radio").on('change', function(e) {
    //$(this).removeClass("btn-outline-secondary");
    //$(this).addClass("btn-primary");

    var vall = $(this).find("input").attr('value');
    $(this).find("input").prop('checked', true);

    get_plan(vall);
});

$("button#budget_source").on('click', function(e) {
    //$(this).removeClass("btn-outline-secondary");
    //$(this).addClass("btn-primary");
    var vall = $(this).find("input").attr('value');
    $(this).find("input").prop('checked', true);

});


$("div#checkCost").on('change', function(e) {
    //alert(activity_plan_id);
    var arrData = {};
    var isCheck = $(this).find("input").is(":checked");
    var checkVal = $(this).find("input").attr('value');
    //console.log(isCheck);
    // alert(valll);
    var eq = checkVal - 1;
    console.log(checkVal);
    if (isCheck == false) {
        $('table#cost tbody tr:eq(' + eq + ')').hide();

        function finding_index(element) { return element.costtype == checkVal; }
        var indexArr = rateArray[0].findIndex(finding_index);
        rateArray[0].splice(indexArr, 1);
        console.log(rateArray[0]);
    } else {

        $('table#cost tbody tr:eq(' + eq + ')').show();

        arrData['costtype'] = checkVal;
        //console.log(arrData);
        rateArray[0].push(arrData);
        console.log(rateArray);
        //alert(rateArray);   


    }

});

$("div#cost_type1,div#cost_type2").on('click', function() {


    $(this).find("input").prop('checked', true);
    var vall = $(this).find("input").attr('id');
    $('#save' + vall + '').prop('checked', true);
    //alert(vall);

});

$('input#price1,input#price2,input#price3,' +
    'input#price4,input#price5,input#price6,input#price7,' +
    'input#price8,input#price9,input#price10').blur(function() {

    var rateVal = $(this).parent().parent().find("input:checked").attr('value');

    var currentVal = $(this).val();

});

$('#target1,#days1,#target2,#days2,#target3,#days3,#target4,#days4,#target5,#days5').blur(function() {
    // alert(this.id.length);
    var num = this.id.substr(this.id.length - 1, 1);
    var rateVal = $(this).parent().parent().find("input:checked").attr('value');
    if ($("#target" + num) !== '' && $("#target" + num) !== '') {
        var targetVal = $("#target" + num).val();
        var daysVal = $("#days" + num).val();
        var sum = targetVal * daysVal * rateVal;
        sumPrice += sum;
        if (sumPrice > plan_budget) {
            alert('คุณกรอกค่าใช้จ่ายเกินงบที่ตั้งไว้ : ที่ตั้งไว้คือ: ' + plan_budget);
            sumPrice -= sum;
            //alert(sumPrice);
            $("button#add_cost_plan").prop("disabled", true);
        } else {
            $("#price" + num).val(sum);
            $("button#add_cost_plan").prop("disabled", false);
        }
    }

});

$('#price5,#price6,#price7,#price8,#price9,#price10,#price11,#price12,#price13').blur(function() {
    var num = this.id.substr(this.id.length - 1, 1);
    var val = Number(this.value);
    if ($("#target" + num).length == 0) {
        sumPrice += val;
        // alert(sumPrice);
        if (sumPrice > plan_budget) {
            alert('คุณกรอกค่าใช้จ่ายเกินงบที่ตั้งไว้ : ที่ตั้งไว้คือ: ' + plan_budget);
            sumPrice -= val;
            $("button#add_cost_plan").prop("disabled", true);
        } else {
            $("button#add_cost_plan").prop("disabled", false);
        }
    }
})


function me() {
    alert('me');
}



function get_plan(val) {

    $('select#select_plan').empty();
    $('select#select_plan').append('<option value="" selected>Choose...</option>');
    $.getJSON('model/get_plan.php', { 'data_type': 'plan', 'id': val }, function(data) {

        $.each(data, function(key, value) {
            //console.log(data);

            $('select#select_plan').append("<option value='" + value.plan_id + "'>" + value.plan_name + "</option>");
        });
    });
};

function get_sub_activity(plan_id) {

    $('select#select_sub_plan').empty();
    $('select#select_sub_plan').append('<option value="" selected>Choose...</option>');
    $.getJSON('model/get_plan.php', { 'data_type': 'sub_activity', 'id': plan_id }, function(data) {

        $.each(data, function(key, value) {
            //console.log(data);

            $('select#select_sub_plan').append("<option value='" + value.sub_activity_id + "'>" + value.sub_activity_name + "</option>");
        });
    });
};

function get_project(e) {
    var plan_id = e.value;
    //alert(plan_id);
    $('select#select_project').empty();
    $('select#select_project').append('<option value="" selected>Choose...</option>');
    $.getJSON('model/get_plan.php', { 'data_type': 'project', 'id': plan_id }, function(data) {
        $.each(data, function(key, value) {
            $('select#select_project').append("<option value='" + value.project_id + "'>" + value.project_name + "</option>");
        });
    });
};


function add_planBasic() {

    var activity_plan_id = $("input#activity_plan_id").val();
    console.log('activity_plan_id:' + activity_plan_id);
    var arrData = {};
    var form = $('form#add_plan_basic');

    var checkStrategy = $("input[name=strategy]").is(":checked"); //return false or true

    var plan = $("select#select_plan option:selected").val(); //unselect return null value
    var project = $("select#select_project option:selected").val();
    var activity_plan = $("input#activity_plan").val();
    var budget = $("input[name=budget]").is(":checked");
    var purpose = $("input#purpose").val();
    var owner = $("input#owner").val();
    console.log($("input[name=budget]").val());
    //alert(JSON.stringify(form));
    if (checkStrategy == false || plan == "" || project == "" || activity_plan == "" || budget == false || owner == "" || purpose == "") {
        alertify.alert("คำเตื่อน กรอกข้อมูลไม่ครบ.", function() {
            //alertify.message('OK');
        });

    } else {
        $.ajax({

            url: 'ajaxData/add_plansBasic.php?activity_plan_id=' + activity_plan_id,
            type: "POST",
            // data: form.serialize() + "&variable=" + jsonString,
            data: form.serialize(),
            dataType: "html",
            success: function(data) {
                console.log(data);
                var obj = JSON.parse('{"AlertList" : ' + data + '}');
                console.log(obj.AlertList[0].return_id);
                if (obj.AlertList[0].result == "Save Success") {

                    //insert  sub
                    console.log('inser_sub befor');
                    addSubActivity(obj.AlertList[0].return_id);

                    $("button#save_plan_basic").prop("disabled", true);
                    $("button#first_step").removeClass('btn-default');
                    $("button#first_step").addClass('btn-primary');

                    activity_plan_id = obj.AlertList[0].return_id;
                    //console.log(activity_plan_id);
                    $("input#activity_plan_id").val(activity_plan_id);

                    //$('div#step2,div#step3').show();
                    $('input#plan_id').val(activity_plan_id);

                    //window.location.href = "index.php";


                } else {
                    alertify.alert('คำเตือน', obj.AlertList[0].result, function() {
                        //alertify.message('OK');
                    });
                }

            },
            error: function(data) {
                alertify.alert("เกิดข้อผิดพลาด ไม่สามารถบันทึกได้", function() {
                    //alertify.message('OK');
                });

            },
        })

    }
}

function addSubActivity(activity_plan_id) {
    console.log('inser_sub indor');
    console.log(activity_plan_id);
    var len_sub_activity = $('input.sub_activity').length;
    var ac_id = $('input.sub_activity_id');
    var ac = $('input.sub_activity');
    var target = $('input.sub_target');
    var purpose = $('input.sub_purpose');
    var mo = $("select.sub_month option:selected");

    for (var i = 1; i <= len_sub_activity; i++) {
        var j = i - 1;
        var obj = {
            "subActId": ac_id.eq(i - 1).val(),
            "subAct": ac.eq(i - 1).val(),
            "subTar": target.eq(i - 1).val(),
            "subPur": purpose.eq(i - 1).val(),
            "monthAct": mo.eq(j).val()
                // "monthAct": $('select#activity_month' + i + ' option:selected').val()
        }
        subActivityArray[0].push(obj);

    }
    //console.log(subActivityArray);

    $.getJSON('model/add_sub_activity.php', {
        'data': subActivityArray[0],
        'plan_id': activity_plan_id
    }, function(data) {
        //var obj = JSON.parse('{"AlertList" : ' + data + '}');
        //console.log(data);
        $.each(data, function(key, value) {
            // alertify.alert('คำแนะนำ', data[0].result, function() {
            //     //alertify.message('OK');
            // });
            alertify.alert('คำแนะนำ', data[0].result, function(e) {
                if (e) {
                    window.location.href = "index.php";
                }
            });


        });
    });

}

function showhideCost(e) {
    // alert(e.id);
    $('table#cost tbody tr').index(0).hide();
    // var m = e.find("input"); //.attr("checked")
    //alert(m);
    // var rows = $('table#cost tr');
    // rows.filter('#cost1').hide();

    // var selected = [];
    // $('div#checkboxes input[type=checkbox]').each(function() {
    //    if ($(this).is(":checked")) {
    //        selected.push($(this).attr('name'));
    //    }
    // });
}

function saveCostPlan() {
    // console.log(rateArray[0].length);
    var select_sub_plan = $('select#select_sub_plan').val();
    if ($('input#activity_plan_id').length > 0) {
        console.log('input active plan Id');
        var activity_plan_id = $("input#activity_plan_id").val();
    } else {
        var activity_plan_id = $("input#addcost_planid").val();
        var sub_plan_id = $("select#select_sub_plan option:selected").val();
        console.log('input addcost plan Id');
    }

    //console.log(activity_plan_id);
    if (select_sub_plan == '') {
        alert('ยังไม่ได้เลือกกิจกรรม');
    } else {
        var i;
        for (i = 1; i <= rateArray[0].length; i++) {
            var elem = rateArray[0][i - 1];
            console.log(elem.costtype);
            //console.log('activity_plan_id:' + 150);

            var tr_cost = $('tr#cost' + elem.costtype);

            var criteria = tr_cost.find("input[name='cost" + elem.costtype + "_criteria']:checked").val();

            var rateVal = tr_cost.find("input[name='cost" + elem.costtype + "save_criteria']:checked").val();
            var rateType = tr_cost.find("input[name='cost" + elem.costtype + "save_criteria']:checked").attr('data-rate_type');

            var target = tr_cost.find("input[name='target" + elem.costtype + "']").val();
            if (target === undefined || target === null) {
                target = '0';
            }

            var days = tr_cost.find("input[name='days" + elem.costtype + "']").val();
            if (days === undefined || days === null) {
                days = '0';
            }

            var price = tr_cost.find("input[name='price" + elem.costtype + "']").val();
            //console.log('criteria:' + criteria + 'rateVal:' + rateVal + 'rateType:' + rateType);

            function finding_index(element) { return element.costtype == elem.costtype; }
            var indexArr = rateArray[0].findIndex(finding_index);
            //console.log(activity_plan_id);

            rateArray[0][indexArr]['sub_plan_id'] = sub_plan_id;
            rateArray[0][indexArr]['activity_plan_id'] = activity_plan_id;
            // rateArray[0][indexArr]['activity_plan_id'] = '101';
            rateArray[0][indexArr]['criteria'] = criteria;
            rateArray[0][indexArr]['rateType'] = rateType;
            rateArray[0][indexArr]['rateVal'] = rateVal;

            rateArray[0][indexArr]['target'] = target;
            rateArray[0][indexArr]['days'] = days;

            rateArray[0][indexArr]['price'] = price;
        }


        //console.log('ratarray00' + rateArray[0][0]['costtype']);
        $.ajax({

            url: 'ajaxData/add_cost_plan.php',
            type: "POST",
            data: {
                'arraycost': rateArray[0],
            },
            dataType: "html",
            // headers: {
            //     'Cache-Control': 'no-cache, no-store, must-revalidate',
            //     'Pragma': 'no-cache',
            //     'Expires': '0'
            // },
            // cache: false,
            success: function(data) {
                // console.log(data);
                var obj = JSON.parse('{"AlertList" : ' + data + '}');
                if (obj.AlertList[0].result == "Save Success") {
                    alertify.alert('คำแนะนำ', obj.AlertList[0].result, function() {
                        //alertify.message('OK');
                    });

                    // $("button#add_cost_plan").prop("disabled", true);
                    $("button#second_step").removeClass('btn-default');
                    $("button#second_step").addClass('btn-primary');
                    // $('#step3').show();

                    for (var i = 0; i < rateArray[0].length; i++) {
                        var eqq = rateArray[0][i]['costtype'];
                        var t = eqq - 1;
                        $('table#cost tbody tr:eq(' + t + ')').hide();
                        $('input#checkbox_cost' + eqq).prop('checked', false);
                        rateArray[0].splice(t, 1);
                    }
                    //rateArray.length = 0;

                    // heare  refresh  cost list
                    get_cost_list_data();


                } else {
                    alertify.alert('คำเตือน', data, function() {
                        //alertify.message('OK');
                    });
                }


            },
            error: function(data) {
                alertify.alert("เกิดข้อผิดพลาด ไม่สามารถบันทึกได้", function() {
                    //alertify.message('OK');
                });

            },
        })
    }
}

function saveImageSms() {
    var form = $('form#image_form')[0];

    var image_name = $('#plan_image').val();
    console.log(image_name);

    if (image_name == '') {
        alert("Please Select Image");
        return false;
    } else {

        var extension = $('#plan_image').val().split('.').pop().toLowerCase();
        if (jQuery.inArray(extension, ['gif', 'png', 'jpg', 'jpeg']) == -1) {

            alert("Invalid Image File");
            $('#plan_image').val('');
            return false;
        } else {
            alert('me');
            $.ajax({
                url: "ajaxData/add_image.php",
                method: "POST",
                data: new FormData(form),
                contentType: false,
                processData: false,
                success: function(data) {
                    console.log(data);
                    //fetch_data();
                    $('#image_form')[0].reset();

                }
            });
        }
    }
}
var countAdd = 1;

function addActivity() {
    console.log('me add plan');
    countAdd += 1;
    var arrData = {};
    //console.log(i);

    var clone = $("div#sub_activity:first").clone();
    clone.find("label").remove();
    clone.find("div#btn_add").remove();
    // clone.children[i].style.fontSize = "12px";
    // console.log(clone.children);

    // $("#sub_activity1", clone).id = "newID";
    clone.find('#sub_activity1').val('');
    clone.find('#sub_activity1').attr('name', 'sub_activity' + countAdd);
    clone.find('#sub_activity1').attr('id', 'sub_activity' + countAdd);

    clone.find('#delAct1').attr('id', 'delAct' + countAdd);

    clone.find('#activity_month1').attr('name', 'activity_month' + countAdd);
    clone.find('#activity_month1').attr('id', 'activity_month' + countAdd);

    clone.find('#sub_activity_target1').val('');
    clone.find('#sub_activity_target1').attr('name', 'sub_activity_target' + countAdd);
    clone.find('#sub_activity_target1').attr('id', 'sub_activity_target' + countAdd);

    clone.find('#sub_activity_purpose1').val('');
    clone.find('#sub_activity_purpose1').attr('name', 'sub_activity_purpose' + countAdd);
    clone.find('#sub_activity_purpose1').attr('id', 'sub_activity_purpose' + countAdd);


    clone.insertAfter("div#sub_activity:last");

}

function delActivity(e) {

    var last1 = e.substr(-1);
    //alert(last1);
    if (last1 > 1) {
        $('#sub_activity' + last1).parents('#sub_activity').remove();
    }

}