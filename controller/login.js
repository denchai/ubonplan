$('document').ready(function() {

    $("form#forgetPass").hide();

    //submit login
    $("button#login").click(function() {

        var username = $("input#username").val().trim();
        var password = $("input#pass").val().trim();
        //alert(username);
        if (username != "" && password != "") {
            $.ajax({
                url: 'check_login.php',
                type: 'post',
                data: { username: username, pass: password },
                success: function(response) {
                    // alert(response);
                    var msg = "";
                    if (response == 1) {
                        window.location = "index.php";
                    } else {

                        //msg = "Invalid username and password!";
                        // alert(response);
                        alertify.alert("ผู้ใช้งาน หรือ รหัสผ่านไม่ถูกต้อง กรุณา login อีกครั้ง", function(e) {
                            if (e) {
                                // user clicked "ok"
                                //window.location.replace("index.php");
                            }

                        });
                    }
                    //$("#message").html(msg);
                }
            });
        }

    });


    /* validation */
    $("#register-form").validate({
        rules: {
            user_name: {
                required: true,
                minlength: 3
            },
            password: {
                required: true,
                minlength: 8,
                maxlength: 15
            },
            cpassword: {
                required: true,
                equalTo: '#password'
            },
            user_email: {
                required: true,
                email: true
            },
        },
        messages: {
            user_name: "Enter a Valid Username",
            password: {
                required: "Provide a Password",
                minlength: "Password Needs To Be Minimum of 8 Characters"
            },
            user_email: "Enter a Valid Email",
            cpassword: {
                required: "Retype Your Password",
                equalTo: "Password Mismatch! Retype"
            }
        },
        submitHandler: submitFormF
    });
    /* validation */

    /* form submit */
    function submitFormF() {

        var data = $("#register-form").serialize();

        $.ajax({

            type: 'POST',
            url: 'register.php',
            data: data,
            beforeSend: function() {
                $("#error").fadeOut();
                $("#btn-submit").html('<span class="glyphicon glyphicon-transfer"></span> &nbsp; sending ...');
            },
            success: function(data) {
                if (data == 1) {

                    $("#error").fadeIn(1000, function() {


                        $("#error").html('<div class="alert alert-danger"> <span class="glyphicon glyphicon-info-sign"></span> &nbsp; Sorry email already taken !</div>');

                        $("#btn-submit").html('<span class="glyphicon glyphicon-log-in"></span> &nbsp; Create Account');

                    });

                } else if (data == "registered") {

                    $("#btn-submit").html('Signing Up');
                    setTimeout('$(".form-signin").fadeOut(500, function(){ $(".signin-form").load("successreg.php"); }); ', 5000);

                } else {

                    $("#error").fadeIn(1000, function() {

                        $("#error").html('<div class="alert alert-danger"><span class="glyphicon glyphicon-info-sign"></span> &nbsp; ' + data + ' !</div>');

                        $("#btn-submit").html('<span class="glyphicon glyphicon-log-in"></span> &nbsp; Create Account');

                    });

                }
            }
        });
        return false;
    }
    /* form submit */

});

function showForgetPass() {
    $("form#login").hide();
    $("form#forgetPass").fadeIn();

}

function showLogin() {
    $("form#login").show();
    $("form#forgetPass").hide();

}

function forgetPassSendMail() {

    var data = $("#forgetPass").serialize();


    $.ajax({

        type: 'POST',
        url: 'loginpage/sendPassword.php',
        data: data,
        beforeSend: function() {
            $("#error").fadeOut();
            $("#btn-sendMail").html('<span class="glyphicon glyphicon-transfer"></span> &nbsp; sending ...');
        },
        success: function(data) {
            if (data != 'Success') {

                $("#error").fadeIn(1000, function() {

                    $("#error").html('<div class="alert alert-danger"> <span class="glyphicon glyphicon-info-sign"></span> &nbsp; Send to your email already::' + data + ' </div>');

                });

            } else {

                $("#error").fadeIn(1000, function() {

                    $("#error").html('<div class="alert alert-danger"><span class="glyphicon glyphicon-info-sign"></span> &nbsp; Have a problem !</div>');

                });

            }
        }
    });
    return false;
}