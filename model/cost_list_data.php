<?php
// Start the session
session_start();
//$_SESSION["vaccation_year"] = "59";
header('Content-Type: text/html; charset=utf-8');
function unset_n_reset(&$arr, $key)
{
    unset($arr[$key]);
    $arr = array_merge($arr);
}
include "../connect.php";
$user_id = $_SESSION["user_id"];

$data = array();

if (isset($_GET['plan_id'])) {
    $plan_id = $_GET['plan_id'];
       //  $plan_id = 66;
        $sql = "select c.*,ct.cost_type_name,s.sub_activity_name from cost_plan_list c
        join sub_activity_plan_list s on s.sub_activity_id=c.sub_activity_id
        join cost_type ct on ct.cost_type_id=c.cost_type_id
            where s.activity_plan_id=$plan_id ";

        if ($result = mysqli_query($con, $sql)) {
            while ($row = mysqli_fetch_array($result, MYSQLI_ASSOC)) {
                $row_array['cost_id'] = $row['cost_id'];
                $row_array['cost_type_id'] = $row['cost_type_id'];
                $row_array['sub_activity_name'] = $row['sub_activity_name'];
                $row_array['criteria_id'] = $row['criteria_id'];
                $row_array['sub_activity_id'] = $row['sub_activity_id'];
                $row_array['date_input'] = $row['date_input'];
                $row_array['cost_type_name'] = $row['cost_type_name'];
                $row_array['target'] = $row['target'];
                $row_array['days'] = $row['days'];
                $row_array['price'] = $row['price'];

                array_push($data, $row_array);

            }
            //for pagiantion
            // $myObj->items = $data;
            // echo json_encode($myObj);
            //for datatable here
            echo json_encode($data);

        }
    
}elseif(isset($_GET['type']) && $_GET['type'] =='del' ){
   
        $cost_id = $_GET['cost_id'];
        $sql = "delete from cost_plan_list   where cost_id=$cost_id ";

        if ($result = mysqli_query($con, $sql)) {           
            $row_array['result'] = "Save Success";
            $row_array['return_id'] = $cost_id;
            array_push($data, $row_array);
        } else {
            $row_array['result'] = "Not Success:";
            $row_array['return_id'] = mysqli_error($con);
            array_push($data, $row_array);
        }
        echo json_encode($data);

    
}

exit;
