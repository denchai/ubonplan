<?php
session_start();
header('Content-Type: text/html; charset=utf-8');
if (!isset($_SESSION['fullname'])) {
    header("location: loginFrm.php");
    exit(0);
}

?>
<!DOCTYPE html>
<html>

<head>
<script src="https://code.jquery.com/jquery-3.4.1.js"
        integrity="sha256-WpOohJOqMqqyKL9FccASB9O0KwACQJpFTUBLTYOVvVU=" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.3/css/bootstrap.min.css"
        integrity="sha384-Zug+QiDoJOrZ5t4lssLdxGhVrurbmBWopoEl+M6BdEfwnCJZtKxi1KgxUyJq13dy" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.11.2/css/all.min.css">



    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"
        integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous">
    </script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"
        integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous">
    </script>

    <link rel="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.css">
    <link rel="https://cdn.datatables.net/1.10.20/css/dataTables.bootstrap4.min.css">





    <style>
    .active {
        padding-top: 3px;
        margin-bottom: -1px;
        margin-top:3px;
        background: #F8F8FF;
        border-radius: 0px;

    }
    .nav>li{
        padding-top: 3px;
        margin-bottom: -1px;
        margin-top:3px;
    }



    .nav>li.active>a {
        color: DodgerBlue !important;
    }


    .nav>li>a:hover{
        color: SlateBlue !important;
    }
    .nav>li:hover>a{
        color: #ff6347 !important;
    }

    /* .nav>li.active>a:after,
    .nav>li:hover>a:after,
    .nav>li>a:hover:after {
        background-color: #5d5d5d !important;
    } */

    .nav>li:hover{
        background-color: LightGray !important;
    }

    table {
        font-size: 14px;
    }
    </style>


    <!-- <script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap4.min.js"></script> -->


    <!-- JavaScript -->
    <script src="//cdn.jsdelivr.net/npm/alertifyjs@1.12.0/build/alertify.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/underscore.js/1.9.1/underscore-min.js"></script>


    <!-- CSS -->
    <link rel="stylesheet" href="//cdn.jsdelivr.net/npm/alertifyjs@1.12.0/build/css/alertify.min.css" />
    <!-- Default theme -->
    <link rel="stylesheet" href="//cdn.jsdelivr.net/npm/alertifyjs@1.12.0/build/css/themes/default.min.css" />
    <!-- Semantic UI theme -->
    <link rel="stylesheet" href="//cdn.jsdelivr.net/npm/alertifyjs@1.12.0/build/css/themes/semantic.min.css" />
    <!-- Bootstrap theme -->
    <link rel="stylesheet" href="//cdn.jsdelivr.net/npm/alertifyjs@1.12.0/build/css/themes/bootstrap.min.css" />

    <!--
    RTL version
-->
    <link rel="stylesheet" href="//cdn.jsdelivr.net/npm/alertifyjs@1.12.0/build/css/alertify.rtl.min.css" />
    <!-- Default theme -->
    <link rel="stylesheet" href="//cdn.jsdelivr.net/npm/alertifyjs@1.12.0/build/css/themes/default.rtl.min.css" />
    <!-- Semantic UI theme -->
    <link rel="stylesheet" href="//cdn.jsdelivr.net/npm/alertifyjs@1.12.0/build/css/themes/semantic.rtl.min.css" />
    <!-- Bootstrap theme -->
    <link rel="stylesheet" href="//cdn.jsdelivr.net/npm/alertifyjs@1.12.0/build/css/themes/bootstrap.rtl.min.css" />
    <link rel="stylesheet" href="css/navbar.css" />





</head>

<body>
    <!-- <div class="container"> -->
    <!-- <h3>แผนปฏิบัติการงานยุทธฯ</h3> -->
    <nav class="navbar navbar-expand-lg navbar-light bg-info">
        <a class="navbar-brand text-light" href="images/icons/lotus.png">
            <img src="images/icons/lotusgreen.png" alt="Smiley face" height="42" width="42">
        </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#menu"
            aria-controls="menu" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="menu">

            <ul class="nav navbar-nav mr-auto" id="me">
                <li class="nav-item nav  active" id="home">
                    <a class="nav-link text-light " id="home" href="index.php?page=home">
                        <!-- <button class="btn btn-warning "> -->
                        <i class="fa fa-fw fa-home fa-1x"></i>
                        หน้าหลัก
                        <!-- </button> -->
                    </a>
                </li>
                <li class="nav nav-item" id="add_plan">
                    <a class="nav-link text-light" id="add_plan" href="index.php?page=add_plan">
                        <!-- <button class="btn btn-warning "> -->
                        <i class="fas fa-file-signature fa-1x"></i>
                        เพิ่มโครงการ
                        <!-- </button> -->
                    </a>
                </li>
                <li class="nav-item dropdown" id="report">
                    <a class="nav-link dropdown-toggle text-light" href="#" id="navbarDropdown" role="button"
                        data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <!-- <button class="btn btn-warning " > -->
                        <i class="far fa-chart-bar fa-1.5x"></i>
                        รายงาน
                        <!-- </button> -->
                    </a>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                        <a class="dropdown-item" href="https://www.jquery-az.com/jquery-tips/">รายงาน1</a>
                        <a class="dropdown-item" href="https://www.jquery-az.com/php-tutorials/">รายงาน2</a>
                        <a class="dropdown-item" href="https://www.jquery-az.com/javascript-tutorials/">รายงาน3</a>
                        <a class="dropdown-item" href="https://www.jquery-az.com/css-tutorials/">รายงาน4</a>
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item" href="https://www.jquery-az.com/alerts-js-bootstrap-jquery/">รายงาน5</a>
                    </div>
                </li>

            </ul>
            <!-- <form class="form-inline my-2 my-lg-0">
                    <input class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search">
                    <button class="btn btn-outline-warning " ><i
                            class="fa fa-fw fa-search"></i>Search</button>
                </form> -->
            <ul class="nav navbar-nav navbar-right">
                <li>
                    <button type="button" class="btn btn-secondary btn-sm">
                        <!-- <i class="far fa-envelope"></i> -->
                        <i class="fas fa-bell"></i>
                        <span class="badge badge-light">4</span>
                    </button>

                </li>
                <li>
                    <div class="btn-group  dropleft">
                        <button type="button" class="btn btn-dark dropdown-toggle btn-sm" data-toggle="dropdown"
                            aria-haspopup="true" aria-expanded="false">
                            <i class="far fa-user"></i><?php echo $_SESSION["fullname"] ?>
                        </button>
                        <div class="dropdown-menu">
                            <a class="dropdown-item" href="profile.php">Profile</a>
                            <a class="dropdown-item" href="logout.php">Another action</a>
                            <a class="dropdown-item" href="logout.php">Logout</a>
                        </div>
                    </div>
                </li>


            </ul>
        </div>
    </nav>





    <div id="tab_include">
        <?php

if (isset($_GET['page']) && $_GET['page'] == 'add_plan') {
    include "add_plan.php";
} elseif (isset($_GET['reportId']) && $_GET['custom_report'] == 'Y') {
    include "specialReport/index.php";
} elseif (isset($_GET['report'])) {
    include "report.php";
} elseif (isset($_GET['createReport'])) {
    include "createReportForm.php";
} elseif (isset($_GET['page']) && $_GET['page'] == 'add_cost') {
    ?>
    <div class="container">
    <?php
include "pages_include/cost_list.php";
    include "pages_include/add_plan_cost.php";
    ?>
    </div>
    <?php
} else {
    include "plans.php";
}

?>

    </div>




    <!-- </div> -->
    <script>
    $(document).ready(function() {

        //alert('addd');

        var ur = window.location.href;
        // alert(ur);
        var pathParts = window.location.href.split('page=');
        var id = pathParts[1];
        // alert(pathParts[1]);
        $('ul.nav li').removeClass('active');
        if (id != '') {
            $('li#' + id).addClass('active');
        } else {
            $('li#home').addClass('active');
        }

        //$('li#add_plan').addClass('active');


        $('ul.nav li a').click(function() {
            //alert(this.id);
            var id = this.id;
            var ur = window.location.href;
            var pathParts = window.location.pathname.split('/');
            var yourLink = pathParts[2];
            var $this = $(this);
        });

    });
    </script>
<script src="helper/helper.js"></script>
</body>

</html>