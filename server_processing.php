<?php
//ชื่อตาราง
$table = 'plan_list';
//ชื่อคีย์หลัก
$primaryKey = 'plan_id';
//ข้อมูลอะเรที่ส่งป datables
$columns = array(
    array('db' => 'plan_id', 'dt' => 0),
    array('db' => 'plan_name', 'dt' => 1),
    array('db' => 'date_input', 'dt' => 2),

    array(
        'db' => 'date_update',
        'dt' => 3,
        'formatter' => function ($d, $row) {
            return date('jS M y', strtotime($d));
        },
    ),
    array('db' => 'user_id', 'dt' => 4),
    array('db' => 'user_id', 'dt' => 5),

);

//เชื่อต่อฐานข้อมูล
$sql_details = array(
    'user' => 'jureerat_pau',
    'pass' => '##pauubon##',
    'db' => 'jureerat_pau',
    'host' => 'claim.phoubon.in.th',
);
// เรียกใช้ไฟล์ spp.class.php
require 'ssp.class.php';

//ส่งข้อมูลกลับไปเป็น JSON
echo json_encode(
    SSP::simple($_GET, $sql_details, $table, $primaryKey, $columns)
);
