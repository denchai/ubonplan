<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.3/css/bootstrap.min.css"
    integrity="sha384-Zug+QiDoJOrZ5t4lssLdxGhVrurbmBWopoEl+M6BdEfwnCJZtKxi1KgxUyJq13dy" crossorigin="anonymous">


<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.11.2/css/all.css">


<link rel="stylesheet" href="../css/A4.css">
<script src="https://code.jquery.com/jquery-3.4.1.js" integrity="sha256-WpOohJOqMqqyKL9FccASB9O0KwACQJpFTUBLTYOVvVU="
    crossorigin="anonymous"></script>

<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"
    integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous">
</script>

<link rel="stylesheet" href="../fonts/sarabun-webfont-master/style.css" />
<style type="text/css">
body {
    background: rgb(204, 204, 204);
}

page {
    background: white;
    display: block;
    margin: 0 auto;
    margin-bottom: 0.5cm;
    box-shadow: 0 0 0.5cm rgba(0, 0, 0, 0.5);
}

page[size="A4"] {
    /* width: 21cm;
    height: 29.7cm; */
    width: 26cm;
    height: 34.7cm;

}

page[size="A4"][layout="landscape"] {
    width: 29.7cm;
    height: 21cm;
}

page[size="A3"] {
    width: 29.7cm;
    height: 42cm;
}

page[size="A3"][layout="landscape"] {
    width: 42cm;
    height: 29.7cm;
}

page[size="A5"] {
    width: 14.8cm;
    height: 21cm;
}

page[size="A5"][layout="landscape"] {
    width: 21cm;
    height: 14.8cm;
}

@media print {

    body,
    page {
        margin: 0;
        box-shadow: 0;
    }
}


table {
    width: 80%;
    border-collapse: collapse;
    margin: auto;
}

table,
th,
td {
    border: 1px solid black;
}


#title {
    font-size: 15px;
    font-weight: bold;
}

.white-space-pre {
    white-space: pre-wrap;
}
.tabIndent{
    text-align: justify;
text-indent: 50px;
}
div.lineH60 {
  line-height: 60%;
}
span {
  display: inline-block;
  width: 400px;
}
</style>
<script>
$(document).ready(function() {




});
</script>


<input type="hidden" name="plan_id" id="plan_id" value="<?php echo $_GET['plan_id'] ?>">

<page size="A4">


    <!-- <div class="container"> -->
        <div class="row">

            <div class="col">
                <img src="../images/krut.jpg" alt="Smiley face" height="50" width="50">
            </div>
            <div class="col">
                <center>
                    <p><b>บันทึกข้อความ</b></p>
                </center>
            </div>

            <div class="col">
                3 of 3
            </div>
        </div>
        <div class="row">

            <div class="col">
                <p>ส่วนราชการ ศูนย์เทคโนโลยีสารสนเทศ สำนักงานสาธารณสุขจังหวัดอุบลราชธานี</p>
                <p>ที่ อบ 0032.07/10</p>
                <p>เรื่อง ส่งหลักฐานการขอเบิกจ่ายงบประมาณต่าง ๆ ตามแผนงาน/โครงการ</p>
                <hr>
                <p>เรียน นายแพทย์สาธารณสุขจังหวัดอุบลราชธานี</p>
            </div>

        </div>
        <div class="row">

            <div class="col">
                <p class="tabIndent">ด้วย เจ้าหน้าที่กลุ่มงาน ศูนย์เทคโนโลยีสารสนเทศ</p>
                <p class="tabIndent">สำนักงานสาธารณสุขอำเภอ....................</p>
          
                <p class="tabIndent">โรงพยาบาลชุมชน...............................</p>
                <p>ได้ส่งหลักฐานการขอเบิกจ่ายเงินมาเพื่อให้สำนักงานสาธารณสุขจังหวัดอุบลราชธานี เบิกจ่ายให้ ได้แก่</p>
        
                
                    <div class="tabIndent"> <span class="tabIndent">ใบสำคัญการจัดซื้อจัดจ้าง</span> <span class="tabIndent">สัญญายื่มเงิน</span>   </div>
             
                    <div class="tabIndent"><span class="tabIndent">รายงานการเดินทางไปราชการ</span>  <span class="tabIndent">ตัดให้พัสดุเพื่อจัดหา วัสดุ/น้ำมัน</span>   </div>
                 
                    <div class="tabIndent"><span class="tabIndent">ใบสำคัญอื่น ๆ ได้แก่.................</span>  <span class="tabIndent">....</span>   </div>
                   

               
            </div>

        </div>

        <div class="row">

            <div class="col lineH60">
                <p>กลุ่มงาน/งาน พัฒนายุทธศาสตร์สาธารณสุข ได้ตรวจสอบหลักฐานการขอเบิกจ่ายเงินดังกล่าวแล้ว</p>
                <p>เห็นว่าเป็นไปตามแผนปฏิบัติการที่ได้รับอนุมัติแล้ว ชื่อ</p>
                <p>เรื่อง ส่งหลักฐานการขอเบิกจ่ายงบประมาณต่าง ๆ ตามแผนงาน/โครงการ</p>
                <p>และให้เบิกจ่ายจาก แหล่งงบประมาณ ดังนี้</p>
              
            </div>

        </div>

        <div class="row">

            <div class="col" border="1">
                <table>
                    <tr>
                        <th>แหล่งงบประมาณ</th>
                        <th>รหัสผลผลิต</th>
                        <th>รหัสกิจกรรมหลัก</th>
                        <th>รหัสแหล่งเงิน</th>
                    </tr>
                    <tr>
                        <td>เงินบำรุง</td>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>เบิกแทนกรม</td>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>PPA</td>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>กองทุน......</td>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>อื่น ๆ</td>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>






                </table>

            </div>

        </div>
        <p></p>

        <div class="row">

            <div class="col lineH60">
                <p class="tabIndent">งบประมาณของโครงการทั้งหมด <span id="budget_request"></span></p>
                <p class="tabIndent">จำนวนที่เบิกจ่ายไปแล้ว</p>
                <p class="tabIndent">จำนวนที่ขอเบิกจ่าย/ยื่มครั้งนี้</p>
                <p class="tabIndent">เมื่อเบิก/ยื่มครั้งนี้แล้ว จะคงเหลือเงินโครงการ</p>

            </div>

        </div>

        <div class="row">
            <div class="col">
                <p>ซึี่งทางกลุ่มงานได้บันทึกคุมงบประมาณแล้ว ขอให้กลุ่มงานพัฒนายุทธศาสตร์สาธารณสุข ตรวจสอบแหล่งงบประมาณ
                    และกลุ่มงานบริหารตรวจสอบความถูกต้องของใบสำคัญ ให้เป็นไปตามระเบี่ยบทางราชการต่อไปด้วย จะเป็นพระคุณ
                </p>
                <p class="tabIndent">จึงเรียนมาเพื่อโปรดพิจารณา</p>      
            </div>
        </div>

        <div class="row">
            <div class="col">
                <center><p >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ลงชื่อ............................       </p></center>
                <center><p >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;(นางหรรษา   ชื่นชูผล)</p> </center>     
                <center><p >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;วัน/เดือน/ปี</p>      </center>
            </div>
        </div>



    <!-- </div> -->




    <?php
include "../connect.php";
// $plan_id = 66;
$plan_id = $_GET['plan_id'];

$sql = "select * from sub_activity_plan_list where activity_plan_id=$plan_id ";

if ($result = mysqli_query($con, $sql)) {
    $i = 0;
    $total = 0;
    while ($row = mysqli_fetch_array($result, MYSQLI_ASSOC)) {

      
     
     
       

    }
 
}

?>

</page>
<script src="../controller/plan_printA4.js"></script>