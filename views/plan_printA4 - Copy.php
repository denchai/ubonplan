<link rel="stylesheet" href="../css/A4.css">
<script src="https://code.jquery.com/jquery-3.4.1.js" integrity="sha256-WpOohJOqMqqyKL9FccASB9O0KwACQJpFTUBLTYOVvVU="
    crossorigin="anonymous"></script>


    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.11.2/css/all.css">
<style type="text/css">
.tg {
    border-collapse: collapse;
    border-spacing: 0;
}

.tg td {
    font-family: Arial, sans-serif;
    font-size: 14px;
    padding: 10px 5px;
    border-style: solid;
    border-width: 1px;
    overflow: hidden;
    word-break: normal;
    border-color: black;
}

.tg th {
    font-family: Arial, sans-serif;
    font-size: 14px;
    font-weight: normal;
    padding: 10px 5px;
    border-style: solid;
    border-width: 1px;
    overflow: hidden;
    word-break: normal;
    border-color: black;
}

.tg .tg-0lax {
    text-align: left;
    vertical-align: top
}

/* ////print A4///// */
body {
    background: rgb(204, 204, 204);
}

page {
    background: white;
    display: block;
    margin: 0 auto;
    margin-bottom: 0.5cm;
    box-shadow: 0 0 0.5cm rgba(0, 0, 0, 0.5);
}

page[size="A4"] {
    width: 21cm;
    height: 29.7cm;
}

page[size="A4"][layout="landscape"] {
    width: 29.7cm;
    height: 21cm;
}

page[size="A3"] {
    width: 29.7cm;
    height: 42cm;
}

page[size="A3"][layout="landscape"] {
    width: 42cm;
    height: 29.7cm;
}

page[size="A5"] {
    width: 14.8cm;
    height: 21cm;
}

page[size="A5"][layout="landscape"] {
    width: 21cm;
    height: 14.8cm;
}

@media print {

    html,  body{
    width: 210mm;
    height: 297mm;
    margin: 0;
    height:100%;
    overflow: hidden;
    background: #FFF;
    font-size: 9.5pt;
   }
   page {
        margin: 0;
        box-shadow: 0;
    }
    .template { width: auto; left:0; top:0; }
  img { width:100%; }
  li { margin: 0 0 10px 20px !important;}
}



table {
    margin: auto;

}
td{
    /* padding-left: 0px;
    padding-right: 0px;
    padding-top:0px;
    padding-bottom:0px; */
}

.tdmultiline {
    overflow: hidden;
    word-wrap: break-word;
}

#title {
    font-size: 15px;
    font-weight: bold;
}

.white-space-pre {
    white-space: pre-wrap;
}
/* //อักษรแนวนอน */
.textAlignVer {
  display: block;
  filter: flipv fliph;
  -webkit-transform: rotate(-90deg);
  -moz-transform: rotate(-90deg);
  transform: rotate(-90deg);
  position: relative;
  /* width: 20px; */
  white-space: nowrap;
 }

</style>
<script>
$(document).ready(function() {


    //console.log("Page path is " + window.location.pathname);
    //console.log("Page path is " + window.location.hostname);
    var costNameWidth = document.getElementsByClassName('costName');
    //console.log(costNameWidth.length);
    for (var i = 0; i < costNameWidth.length; i++) {
        //console.log(costNameWidth[i].offsetHeight);
        costPrice[i].style.height = costNameWidth[i].offsetHeight + 'px';
    }

    var rotates = document.getElementsByClassName('textAlignVer');
    for (var i = 0; i < rotates.length; i++) {
        //console.log(rotates[i].offsetWidth);
        rotates[i].style.height = rotates[i].offsetWidth + 'px';
    }



});
</script>
 <style type="text/css">
    .tg {
       /*border-collapse: collapse;*/
        border-spacing: 0;
    }

    .tg td {
        font-family: Arial, sans-serif;
        font-size: 12px;
        padding: 5px 5px;
        /* border-style: solid; */
        /* border-width: 1px; */
        overflow: hidden;
        word-break: normal;
        border-color: black;
    }

    .tg th {
        font-family: Arial, sans-serif;
        font-size: 14px;
        font-weight: normal;
        padding: 10px 5px;
        /* border-style: solid; */
        /* border-width: 1px; */
        overflow: hidden;
        word-break: normal;
        /* border-color: black; */
    }

    .tg .tg-0pky {
        /* border-color: inherit; */
        text-align: left;
        vertical-align: top
    }

    .tg .tg-0lax {
        text-align: left;
        vertical-align: top
    }
    p{
        line-height: 80%;
    }
    td{

        /* vertical-align: text-top; */
    }
    div#me{
        vertical-align: text-top;
        /* border: solid; */
        margin-top:0px;
    }
    #mm{
        float: right;
    }
    #m{
        float: left;
    }

    </style>
<input type="hidden" name="plan_id" id="plan_id" value="<?php echo $_GET['plan_id'] ?>">

<page size="A4" layout="landscape">

    <br>

    <table class="tg" width="95%" align="center" id="inform">
        <thead>
            <tr>
                <th class="tg-0lax" colspan="4">
                    <p align="center">แผนปฏิบัติการพัฒนางานสาธารณสุข
                        เครือข่ายบริการสุขภาพอำเภอ............................ประจำปีงบปรมาณ พ.ศ. 2563</p>
                </th>
            </tr>
            <tr>
                <th class="tg-0lax" colspan="4">

                    <p><span id="title">ยุทธศาตร์กระทรวงสาธารณสุข: </span>&nbsp;&nbsp;&nbsp;<span
                            id="strategy_name"></span></p>
                </th>
            </tr>
            <tr>
                <th class="tg-0lax" colspan="2">


                    <p><span id="title">แผนงาน </span><span id="plan_name"></span></p>
                    <p><span id="title">โครงการ </span><span id="project_name"></span></p>
                    <p><span id="title">ชื่อโครงการ </span><span id="subject_name"></span></p>
                    <p>
                    <span id="title">แหล่งงบประมาณ </span>
                    <span id="budget_name"></span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <span id="title">งบประมาณที่ตั้งไว้ </span>
                    <span id="budget_request"></span>
                    </p>
                </th>
                <th class="tg-0lax" colspan="2">
                    <p><span id="title">วัตถุประสงค์</span><span id="purpose">
                            <div class="white-space-pre" id="t"></div>
                        </span></p>


                    <p><span id="title">ผู้รับผิดชอบ </span><span id="owners"></span></p>

                </th>
            </tr>

        </thead>
    </table>


    <table class="tg" width="95%" align="center" style="margin-top='10px'">
    <thead>
        <tr>
            <th class="tg-0pky" rowspan="3">ลำดับ</th>
            <th class="tg-0pky" rowspan="3">โครงการและกิจกรรมดำเนินงาน</th>
            <th class="tg-0pky" rowspan="3">วัตถุประสงค์</th>
            <th class="tg-0pky" rowspan="3">กลุ่มเป้าหมาย</th>
            <th class="tg-0pky" colspan="4" rowspan="2">ช่วงเวลาดำเนินการ (ไตรมาส)</th>
            <th class="tg-0pky" colspan="2" rowspan="2">งบประมาณ</th>
            <th class="tg-0lax" rowspan="3">งปม.รวม (บาท)</th>
            <th class="tg-0lax" colspan="12">งบประมาณ รายไตรมาส (บาท)</th>
            <!-- <th class="tg-0pky" rowspan="3">ผู้รับผิดชอบ</th>
            <th class="tg-0lax" rowspan="3">แหล่งงบประมาณ</th> -->
        </tr>
        <tr>
            <td class="tg-0lax" colspan="3">ไตรมาส 1</td>
            <td class="tg-0lax" colspan="3">ไตรมาส 2</td>
            <td class="tg-0lax" colspan="3">ไตรมาส 3</td>
            <td class="tg-0lax" colspan="3">ไตรมาส 4</td>
        </tr>
        <tr>
            <td class="tg-0pky">1</td>
            <td class="tg-0lax">2</td>
            <td class="tg-0lax">3</td>
            <td class="tg-0lax">4</td>
            <td class="tg-0pky">รายละเอียด</td>
            <td class="tg-0lax">บาท</td>
            <td class="tg-0lax">ตค</td>
            <td class="tg-0lax">พย</td>
            <td class="tg-0pky">ธค</td>
            <td class="tg-0pky">มค</td>
            <td class="tg-0lax">กพ</td>
            <td class="tg-0lax">มีค</td>
            <td class="tg-0lax">เมย</td>
            <td class="tg-0lax">พค</td>
            <td class="tg-0lax">มิย</td>
            <td class="tg-0lax">กค</td>
            <td class="tg-0lax">สค</td>
            <td class="tg-0lax">กย</td>
        </tr>
        </thead>
        <tbody>
        <?php
include "../connect.php";
// $plan_id = 66;
$plan_id = $_GET['plan_id'];

$sql = "select * from sub_activity_plan_list where activity_plan_id=$plan_id ";

if ($result = mysqli_query($con, $sql)) {
    $i = 0;
    $total = 0;
    while ($row = mysqli_fetch_array($result, MYSQLI_ASSOC)) {

        $i++;
        echo "<tr>";
        echo "<td>$i</td>";
        echo "<td>" . $row['sub_activity_name'] . "</td>";
        echo "<td>" . $row['sub_activity_purpose'] . "</td>";
        echo "<td >" . $row['sub_activity_target'] . "</td>";

        echo "<td>";
        if (in_array($row['month_activity'], [10, 11, 12])) {
            echo "<i class='far fa-check-circle fa-sm'>";
        }
        echo "</td>";
        echo "<td>";
        if (in_array($row['month_activity'], [1, 2, 3])) {
            echo "<i class='far fa-check-circle fa-sm'>";
        }
        echo "</td>";
        echo "<td>";
        if (in_array($row['month_activity'], [4, 5, 6])) {
            echo "<i class='far fa-check-circle fa-sm'>";
        }
        echo "</td>";
        echo "<td>";
        if (in_array($row['month_activity'], [7, 8, 9])) {
            echo "<i class='far fa-check-circle fa-sm'>";
        }
        echo "</td>";
        // ประเภทค่าใช้จ่าย
        $sql_cost = "select ct.cost_type_name,sum(cl.price) as total from cost_plan_list cl join cost_type ct on ct.cost_type_id=cl.cost_type_id
        where cl.sub_activity_id=" . $row['sub_activity_id'] . " GROUP BY cl.sub_activity_id,cl.cost_type_id ";
        // $result_cost = mysqli_query($con, $sql_cost);
        $result_cost = mysqli_query($con, $sql_cost);
        $rowcount = mysqli_num_rows($result_cost);
        if ($rowcount > 0) {

            echo "<td >";
            while ($row_cost = mysqli_fetch_array($result_cost, MYSQLI_ASSOC)) {
                echo "<div id='costName' class='costName'>- ";
                echo $row_cost['cost_type_name'];
                echo "</div>";
            }
            echo "</td>";
            $result_total = mysqli_query($con, $sql_cost);
            echo "<td align='right'>";
            $sumBySubAct = 0;
            while ($row_total = mysqli_fetch_array($result_total, MYSQLI_ASSOC)) {
                echo "<div id='costPrice'  class='costPrice'>" . number_format($row_total['total']) . "</div>";
                $sumBySubAct += $row_total['total'];
            }
            echo "</td>";
        } else {
            echo "<td ></td><td ></td>";

        }

        echo "<td align='right'>" . number_format($sumBySubAct) . "</td>";
        if ($row['month_activity'] == 10) {$val10 = number_format($sumBySubAct);} else { $val10 = '';}
        if ($row['month_activity'] == 11) {$val11 = number_format($sumBySubAct);} else { $val11 = '';}
        if ($row['month_activity'] == 12) {$val12 = number_format($sumBySubAct);} else { $val12 = '';}
        if ($row['month_activity'] == 1) {$val1 = number_format($sumBySubAct);} else { $val1 = '';}
        if ($row['month_activity'] == 2) {$val2 = number_format($sumBySubAct);} else { $val2 = '';}
        if ($row['month_activity'] == 3) {$val3 = number_format($sumBySubAct);} else { $val3 = '';}
        if ($row['month_activity'] == 4) {$val4 = number_format($sumBySubAct);} else { $val4 = '';}
        if ($row['month_activity'] == 5) {$val5 = number_format($sumBySubAct);} else { $val5 = '';}
        if ($row['month_activity'] == 6) {$val6 = number_format($sumBySubAct);} else { $val6 = '';}
        if ($row['month_activity'] == 7) {$val7 = number_format($sumBySubAct);} else { $val7 = '';}
        if ($row['month_activity'] == 8) {$val8 = number_format($sumBySubAct);} else { $val8 = '';}
        if ($row['month_activity'] == 9) {$val9 = number_format($sumBySubAct);} else { $val9 = '';}

        echo "<td class='textAlignVer'>" . $val10 . " </td>";
        echo "<td class='textAlignVer'>" . $val11 . " </td>
                <td class='textAlignVer'>" . $val12 . " </td><td class='textAlignVer'>" . $val1 . " </td>
                <td class='textAlignVer'>" . $val2 . " </td><td class='textAlignVer'>" . $val3 . " </td>
                <td class='textAlignVer'>" . $val4 . " </td><td class='textAlignVer'>" . $val5 . " </td>
                <td class='textAlignVer'>" . $val6 . " </td><td class='textAlignVer'>" . $val7 . " </td>
                <td class='textAlignVer'>" . $val8 . " </td><td class='textAlignVer'>" . $val9 . " </td>";

        echo "</tr>";

        //echo "</tr>";
        $total += $sumBySubAct;

    }
    echo "<tr>";
    echo "<td colspan='3' style='text-align: right;'>รวมค่าใช้จ่ายทั้งหมด</td>";

    echo "<td colspan='20' style='text-align: right;'>" . number_format($total) . " บาท</td>";

    echo "</tr>";
}

?>

        </tbody>
    </table>
</page>
<script src="../controller/plan_printA4.js"></script>