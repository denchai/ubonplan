<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.3/css/bootstrap.min.css"
    integrity="sha384-Zug+QiDoJOrZ5t4lssLdxGhVrurbmBWopoEl+M6BdEfwnCJZtKxi1KgxUyJq13dy" crossorigin="anonymous">


<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.11.2/css/all.css">

<script src="https://code.jquery.com/jquery-3.4.1.js" integrity="sha256-WpOohJOqMqqyKL9FccASB9O0KwACQJpFTUBLTYOVvVU="
    crossorigin="anonymous"></script>

<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"
    integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous">
</script>

<link rel="stylesheet" href="../fonts/sarabun-webfont-master/style.css" />
<style type="text/css">
//////print A4////////


body {
    width: 100%;
    height: 100%;
    margin: 0;
    padding: 0;
    /* background-color: #FAFAFA; */
    /* font: 12pt "Tahoma"; */

    font-family: 'THSarabunNew', sans-serif;
    font-size: 1em;
    line-height: 0.30em;
    background: #e1e1e1;
}

* {
    box-sizing: border-box;
    -moz-box-sizing: border-box;
}

.page {
    width: 210mm;
    min-height: 290mm;
    padding: 2mm;
    margin: 1mm auto;
    border: 1px #D3D3D3 solid;
    border-radius: 5px;
    background: white;
    box-shadow: 0 0 5px rgba(0, 0, 0, 0.1);
}

.subpage {
    padding: 0cm;
    /* border: 5px red solid; */
    height: 290mm;
    /* outline: 2cm #FFEAEA solid; */

    font-family: 'THSarabunNew', sans-serif;
    font-size: 1em;
    line-height: 0.30em;
}

@page {
    size: A4;
    margin: 0;
}

@media print {

    html,
    body {
        width: 210mm;
        height: 297mm;
    }

    .page {
        margin: 0;
        border: initial;
        border-radius: initial;
        width: initial;
        min-height: initial;
        box-shadow: initial;
        background: initial;
        page-break-after: always;
    }
}

/* ///end of print a4/// */

table {
    width: 80%;
    border-collapse: collapse;
    margin: auto;
    font-family: 'THSarabunNew', sans-serif;
    font-size: 1em;

}

table,
th,
td {
    border: 1px solid black;

}


#title {
    font-size: 15px;
    font-weight: bold;
}

.white-space-pre {
    white-space: pre-wrap;
}

.tabIndent {
    text-align: justify;
    text-indent: 50px;
}

div.lineH60 {
    /* line-height: 50%; */
}

span {
    display: inline-block;
    width: 400px;
}

table.items,  td.items {
    border: none;
    /* padding:5px; */
}
b#squre{
    /* float: left; */
    font-size: 2em;
    line-height: 0.30em;
}
div#right-title{
    padding-top:18px;
    padding-bottom:1px;
    border: 1px  solid;
    vertical-align: text-bottom;
}
</style>
<script>
$(document).ready(function() {




});
</script>
<div class="book">
    <div class="page">
        <div class="subpage">

            <input type="hidden" name="plan_id" id="plan_id" value="<?php echo $_GET['plan_id'] ?>">
            <br>
            <br>
            <br>
            <br>


            <div class="container">
                <div class="row">

                    <div class="col">
                    <p></p>
                    <p></p>

                        <img src="../images/krut.jpg" alt="Smiley face" height="50" width="50">
                    </div>
                    <div class="col" style="vertical-align: text-bottom;">
                    <p></p>
                    <p></p>
                    <p></p>

                            <p style="vertical-align: text-bottom;font-size:1.2em"><b > บันทึกข้อความ</b></p>

                    </div>

                    <div class="col" id="right-title">
                        <p>ก.ยุทธศาสตร์ ตรวจสอบแล้ว</p>
                        <p>ผู้ตรวจสอบ................</p>
                        <p>ว/ด/ป................</p>
                    </div>
                </div>
                <br>
                <br>
                <div class="row">

                    <div class="col">
                        <p>ส่วนราชการ ศูนย์เทคโนโลยีสารสนเทศ สำนักงานสาธารณสุขจังหวัดอุบลราชธานี</p>
                        <p>ที่ อบ 0032.07/10</p>
                        <p>เรื่อง ส่งหลักฐานการขอเบิกจ่ายงบประมาณต่าง ๆ ตามแผนงาน/โครงการ</p>
                        <hr>
                        <p>เรียน นายแพทย์สาธารณสุขจังหวัดอุบลราชธานี</p>
                    </div>

                </div>
                <div class="row">

                    <div class="col ">
                        <div class="lineH60">
                            <p class="tabIndent">ด้วย เจ้าหน้าที่กลุ่มงาน ศูนย์เทคโนโลยีสารสนเทศ</p>
                            <p class="tabIndent">สำนักงานสาธารณสุขอำเภอ....................</p>

                            <p class="tabIndent">โรงพยาบาลชุมชน...............................</p>
                        </div>
                        <p>ได้ส่งหลักฐานการขอเบิกจ่ายเงินมาเพื่อให้สำนักงานสาธารณสุขจังหวัดอุบลราชธานี เบิกจ่ายให้
                            ได้แก่</p>
                        <table width="80%" border="0" class="items">
                            <tbody>
                                <tr>
                                    <td class="items"><b id="squre">&#9633;</b>&nbsp;ใบสำคัญการจัดซื้อจัดจ้าง</td>
                                    <td class="items"><b id="squre">&#9633;</b>&nbsp;สัญญายื่มเงิน</td>
                                </tr>
                                <tr>
                                    <td class="items"><b id="squre">&#9633;</b>&nbsp;รายงานการเดินทางไปราชการ</td>
                                    <td class="items"><b id="squre">&#9633;</b>&nbsp;ตัดให้พัสดุเพื่อจัดหา วัสดุ/น้ำมัน<</td>
                                </tr>
                                <tr>
                                    <td class="items"><b id="squre">&#9633;</b>&nbsp;ใบสำคัญอื่น ๆ ได้แก่.................</td>
                                    <td class="items">&.......................</td>
                                </tr>
                            </tbody>
                        </table>

                </div>


            </div>

            <p></p>
            <div class="row">

                <div class="col lineH60">
                    <p>กลุ่มงาน/งาน พัฒนายุทธศาสตร์สาธารณสุข ได้ตรวจสอบหลักฐานการขอเบิกจ่ายเงินดังกล่าวแล้ว   เห็นว่าเป็นไปตามแผนปฏิบัติการ</p>
                    <p>ที่ได้รับอนุมัติแล้ว ชื่อ</p>
                    <p>และให้เบิกจ่ายจาก แหล่งงบประมาณ ดังนี้</p>

                </div>

            </div>

            <div class="row">

                <div class="col" border="1">
                    <table>
                        <tr>
                            <th>แหล่งงบประมาณ</th>
                            <th>รหัสผลผลิต</th>
                            <th>รหัสกิจกรรมหลัก</th>
                            <th>รหัสแหล่งเงิน</th>
                        </tr>
                        <tr>
                            <td>เงินบำรุง</td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>เบิกแทนกรม</td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>PPA</td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>กองทุน......</td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>อื่น ๆ</td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                    </table>

                </div>

            </div>
            <p></p>

            <div class="row">

                <div class="col lineH60">
                    <div class="tabIndent">
                        <p class="tabIndent">งบประมาณของโครงการทั้งหมด <span id="budget_request"></span></p>
                    </div>
                    <div class="tabIndent">
                        <p class="tabIndent">จำนวนที่เบิกจ่ายไปแล้ว</p>
                    </div>
                    <div class="tabIndent">
                        <p class="tabIndent">จำนวนที่ขอเบิกจ่าย/ยื่มครั้งนี้</p>
                    </div>
                    <div class="tabIndent">
                        <p class="tabIndent">เมื่อเบิก/ยื่มครั้งนี้แล้ว จะคงเหลือเงินโครงการ</p>
                    </div>

                </div>

            </div>

            <div class="row">
                <div class="col">
                    <p>ซึี่งทางกลุ่มงานได้บันทึกคุมงบประมาณแล้ว ขอให้กลุ่มงานพัฒนายุทธศาสตร์สาธารณสุข
                        ตรวจสอบแหล่งงบประมาณ</p>
                     <p>   และกลุ่มงานบริหารตรวจสอบความถูกต้องของใบสำคัญ ให้เป็นไปตามระเบี่ยบทางราชการต่อไปด้วย
                        จะเป็นพระคุณ
                    </p>
                    <br>
                    <p class="tabIndent">จึงเรียนมาเพื่อโปรดพิจารณา</p>
                </div>
            </div>

            <div class="row">
                <div class="col">
                    <center>
                        <p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ลงชื่อ............................
                        </p>
                    </center>
                    <center>
                        <p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;(นางหรรษา
                            ชื่นชูผล)</p>
                    </center>
                    <center>
                        <p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;วัน/เดือน/ปี
                        </p>
                    </center>
                </div>
            </div>



        </div>

    </div>
        <?php
include "../connect.php";
// $plan_id = 66;
$plan_id = $_GET['plan_id'];

$sql = "select * from sub_activity_plan_list where activity_plan_id=$plan_id ";

if ($result = mysqli_query($con, $sql)) {
    $i = 0;
    $total = 0;
    while ($row = mysqli_fetch_array($result, MYSQLI_ASSOC)) {

    }

}

?>


        <script src="../controller/plan_printA4.js"></script>

</div>

