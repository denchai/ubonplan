<link href="vendor/pagination/pagination.css" rel="stylesheet" type="text/css">
<style type="text/css">
ul,
li {
    list-style: none;
}

#wrapper {
    width: 900px;
    margin: 20px auto;
}

.data-container {
    margin-top: 20px;
}

.data-container ul {
    padding: 0;
    margin: 0;
}

.data-container li {
    margin-bottom: 5px;
    padding: 5px 10px;
    background: #eee;
    color: #666;
}


ul li {font-weight: bold; margin: 15px 0;}
ul ul {list-style-type: square;}
ul ul li {font-weight: normal; color: #333; margin: 5px 15px;}
</style>

<!-- the form to be viewed as dialog-->
<input type="hidden" name="plan_id" id="plan_id" value="<?php echo $_GET['plan_id'] ?>">
<form id="editForm">
    <!-- <table id="editCost" class="table table-bordered table-hover">

    </table> -->




    <input type="hidden" name="cost_id" id="cost_id" value="" />




</form>
<div class="card" id="step2">
    <div class="card-header">
        <div class="stepwizard-step">
            <button id="first_step" type="button" class="btn btn-success btn-circle"></button>
            ค่าใช้จ่ายที่เบิกไปแล้ว
        </div>
    </div>
    <div class="card-body">
        <table class="table table-hover table-sm table-striped" id="cost_list">
            <thead>
                <tr>
                    <!-- <th>กิจกรรม</th>
                    <th>วันที่เบิก</th> -->
                    <th>ประเภทค่าใช้จ่าย</th>
                    <th>วันที่เบิก</th>
                    <th>เป้าหมาย(คน)</th>
                    <th>จำนวนวัน</th>
                    <th>รวมเงิน</th>
                    <th>#</th>
                </tr>
            </thead>
            <tbody>

            </tbody>
        </table>
        <div id="results"></div>
    </div>
</div>


<script src="https://code.jquery.com/jquery-3.4.1.js" integrity="sha256-WpOohJOqMqqyKL9FccASB9O0KwACQJpFTUBLTYOVvVU="
    crossorigin="anonymous"></script>
<!-- <script src="vendor/pagination/pagination.min.js"></script> -->
<script>
$(document).ready(function() {
    get_cost_list_data();





})
function get_cost_list_data() {
    var plan_id = $('input#plan_id').val();
    $('table#cost_list tbody').empty();
    //alert(plan_id);
    $('select#select_project').empty();
    $('select#select_project').append('<option value="" selected>Choose...</option>');
    $.getJSON('model/cost_list_data.php', { 'plan_id': plan_id }, function(data) {

        /////////////convert data object some value  to interger/////////////////////

        for(var i = 0; i < data.length; i++){
            var obj = data[i];
            for(var prop in obj){
                if(obj.hasOwnProperty(prop) && obj[prop] !== null && !isNaN(obj[prop])){
                    obj[prop] = +obj[prop];
                }
            }
        }

        /////////////////////////////////

        var categories = {},
        groupBy = "sub_activity_name",
        sumBy = "price",
        ul = $('<ul>');

        for (var i = 0; i < data.length; i++) {
            if (!categories[data[i][groupBy]])

                categories[data[i][groupBy]] = [];
            categories[data[i][groupBy]].push(data[i]);
        };

        for (key in categories) {
            if (categories.hasOwnProperty(key)) {
                let sum = categories[key].reduce((s, f) => s + f.price, 0);

                $('table#cost_list tbody').append("<tr><td colspan='4'><b>"+key+"</b></td><td><b>"+sum.toLocaleString()+"<b></td><td></td></tr>");
                if (categories[key].length){
                    // var ul_inner = $('<ul>');
                    for (var i = 0; i < categories[key].length; i++) {
                        $('table#cost_list tbody').append(

                            "<tr>"+
                                "<td style='padding-left:2.8em'>"+categories[key][i].cost_type_name+"</td>"+
                                "<td> "+categories[key][i].date_input+"</td>"+
                                "<td >"+categories[key][i].target+"</td>"+
                                "<td >"+categories[key][i].days+"</td>"+
                                "<td >"+(categories[key][i].price).toLocaleString()+"</td>"+
                                "<td >&nbsp<button type='button'  class='btn btn-warning btn-sm'  id='" + categories[key][i].cost_id + "' onclick='delCost(this)'>ลบ</button></td></tr>"

                            )
                    }

                }

            }
        }

    });
};





function editCostrow(e) {
    $('table#editCost > tr').remove();
    //console.log($(e).parent().parent().children('td').eq(0).text()) ;
    var strClick = e.getAttribute("data-row");
    console.log(strClick);
    var ar = strClick.split('_');
    console.log(ar);
    var rowIndex = ar[0] - 1;
    var trCostid = ar[0];
    console.log(rowIndex);
    console.log('trCostid' + trCostid);
    var table = $("table#cost > tbody >tr ");
    var lenTable = table.length;

    var row = table[rowIndex];
    var clone = row.cloneNode(true);
    clone.style.display = "block";

    // console.log(lenTable);
    // console.log('row' + row);


    $('table#editCost').append(clone);

    var target = $(e).parent().parent().children('td').eq(3).text();
    var days = $(e).parent().parent().children('td').eq(4).text();
    var price = $(e).parent().parent().children('td').eq(5).text();
    $('input#target' + trCostid + '').val(target);
    $('input#days' + trCostid + '').val(days);
    $('input#price' + trCostid + '').val(price);

    alertify.genericDialog || alertify.dialog('genericDialog', function() {
        return {
            main: function(content) {
                this.setContent(content);
            },
            setup: function() {
                return {
                    focus: {
                        element: function() {
                            return this.elements.body.querySelector(this.get('selector'));
                        },
                        select: true
                    },
                    options: {
                        basic: true,
                        maximizable: false,
                        resizable: false,
                        padding: false
                    }
                };
            },
            settings: {
                selector: undefined
            },
            hooks: {
                onshow: function() {
                    this.elements.dialog.style.maxWidth = 'none';
                    this.elements.dialog.style.width = '80%';
                },
                onclose: function() {
                    alertify.dialog.closeAll;

                }
            },
        };
    });
    //force focusing password box

    alertify.genericDialog($('#editForm')[0]).set('selector', 'input[type="password"]');
}

function editCost() {
    //console.log('me');
    var cost_id = $("input#cost_id").val();
    var form = $('form#editForm');

    var target = $("input#target").val();
    var days = $("input#days").val();

    if (target == "" || days == "") {
        alertify.alert("คำเตื่อน กรอกข้อมูลไม่ครบ.", function() {
            //alertify.message('OK');
        });

    } else {

        $.ajax({

            url: 'ajaxData/add_plansBasic.php?activity_plan_id=' + activity_plan_id,
            type: "POST",
            data: form.serialize(),
            dataType: "html",
            // headers: {
            //     'Cache-Control': 'no-cache, no-store, must-revalidate',
            //     'Pragma': 'no-cache',
            //     'Expires': '0'
            // },
            // cache: false
            success: function(data) {
                // alert(data);
                var obj = JSON.parse('{"AlertList" : ' + data + '}');


                // ฝฝ$.each(data, function(key, value) {
                //alert(obj.TeamList[0].result);

                if (obj.AlertList[0].result == "Save Success") {
                    alertify.alert('คำแนะนำ', obj.AlertList[0].result, function() {
                        //alertify.message('OK');
                    });
                    $("button#save_plan_basic").prop("disabled", true);
                    $("button#first_step").removeClass('btn-default');
                    $("button#first_step").addClass('btn-primary');

                    activity_plan_id = obj.AlertList[0].return_id;

                    $('div#step2,div#step3').show();
                    $('input#plan_id').val(activity_plan_id);
                    alertify.closeAll();

                } else {
                    alertify.alert('คำเตือน', obj.AlertList[0].result, function() {
                        //alertify.message('OK');
                    });
                }

            },
            error: function(data) {
                alertify.alert("เกิดข้อผิดพลาด ไม่สามารถบันทึกได้", function() {
                    //alertify.message('OK');
                });

            },
        })

    }
}

function delCost(e) {

    var cost_id = e.id;

    // console.log(cost_id);

    alertify.confirm('คำเตือน', 'คุณต้องการจะลบรายการนี้หรือไม่', function() {

        $.getJSON('model/cost_list_data.php', {
            'type': 'del',
            'cost_id': cost_id
        }, function(data) {
            //var obj = JSON.parse('{"AlertList" : ' + data + '}');

            $.each(data, function(key, value) {
                console.log(value.result);
                if (value.result == 'Save Success') {
                    get_cost_list_data();
                    alertify.success('Ok Delete Cost Id: ' + value.return_id);
                }


            });

        });


    }, function() {
        alertify.error('Cancel')
    });

}
</script>