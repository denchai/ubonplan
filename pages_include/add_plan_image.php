<div class="card" id="step3">
    <div class="card-header">
        <div class="stepwizard-step">
            <button id="first_step" type="button" class="btn btn-success btn-circle">3</button>
            อัพโหลดรุป SMS
        </div>
    </div>
    <div class="card-body">
        <form id="image_form">
            <p id="old_img_txt"></p>
            <div id="image_sms"></div>
            <div class="form-group">
            <img id="img_preview" src="images/No_sign.png" alt="your image" height="100" />
                <p id="new_img_txt">..</p>
                <label for="plan_image">กรุณาเลือกไฟล์ภาพ จากระบบ SMS</label>

                <input type="file" class="form-control-file" id="plan_image" name="image" plac>
                <input type="hidden" name="plan_id_img" id="plan_id_img" value="">
                <input type="hidden" name="action" value="insert">
            </div>

            <div class="form-group row">
                <div class="col-sm-10">
                    <button type="button" class="btn btn-primary" onclick="saveImageSms()">บันทึก</button>

                </div>
            </div>

        </form>
    </div>
</div>
