<form id="add_plan_basic">
    <input type="hidden" name="activity_plan_id" id="activity_plan_id" value="">
    <div class="form-group row">
        <label for="inputEmail3" class="col-sm-2 col-form-label">ยุทธศาสตร์กระทรวง</label>
        <div class="col-sm-10">
            <!-- Default inline 1-->
            <button type="button" class="btn btn-outline-secondary" id="strategy_radio">
                <div class="custom-control custom-radio custom-control-inline">
                    <input type="radio" class="custom-control-input" id="strategy1" name="strategy" value="1">
                    <label class="custom-control-label" for="strategy1">E1</label>
                </div>
            </button>

            <!-- Default inline 2-->
            <button type="button" class="btn btn-outline-secondary" id="strategy_radio">
                <div class="custom-control custom-radio custom-control-inline">
                    <input type="radio" class="custom-control-input" id="strategy2" name="strategy" value="2"
                        onchange="showthis(this)">
                    <label class="custom-control-label" for="strategy2">E2</label>
                </div>
            </button>

            <!-- Default inline 3-->
            <button type="button" class="btn btn-outline-secondary" id="strategy_radio">
                <div class="custom-control custom-radio custom-control-inline ">
                    <input type="radio" class="custom-control-input" id="strategy3" name="strategy" value="3">
                    <label class="custom-control-label" for="strategy3">E3</label>
                </div>
            </button>
            <!-- Default inline 3-->
            <button type="button" class="btn btn-outline-secondary" id="strategy_radio">
                <div class="custom-control custom-radio custom-control-inline">
                    <input type="radio" class="custom-control-input" id="strategy4" name="strategy" value="4">
                    <label class="custom-control-label" for="strategy4">E4</label>
                </div>
            </button>

        </div>
    </div>
    <div class="form-group row">
        <label for="inputEmail3" class="col-sm-2 col-form-label">แผนงาน</label>
        <div class="col-sm-10">
            <select class="custom-select" id="select_plan" name="select_plan" onchange="get_project(this)">
                <option value="" selected>Choose...</option>
                <!-- <option value="1">One</option>
                      <option value="2">Two</option>
                      <option value="3">Three</option> -->
            </select>
        </div>
    </div>
    <div class="form-group row">
        <label for="inputEmail3" class="col-sm-2 col-form-label">โครงการ</label>
        <div class="col-sm-10">
            <select class="custom-select" id="select_project" name="select_project">
                <option value="" selected>Choose...</option>
                <!-- <option value="1">One</option>
                      <option value="2">Two</option>
                      <option value="3">Three</option> -->
            </select>
        </div>
    </div>


    <div class="form-group row">
        <label for="inputEmail3" class="col-sm-2 col-form-label">ชื่อโครงการ</label>
        <div class="col-sm-10">
            <input type="text" class="form-control" id="activity_plan" name="activity_plan"
                placeholder="ชื่อโครงการและกิจกรรมดำเนินงาน">
        </div>
    </div>

    <div class="form-group row">
        <label for="inputEmail3" class="col-sm-2 col-form-label">กิจกรรม
            <button type="button" id="inputZip" class="btn btn-primary mb-2 " onclick="addActivity()">++เพิ่ม</button>

        </label>
        <div class="col-sm-10">

            <div class="form-row" id="sub_activity">
                <div class="form-group col-md-9">
                    <input type="hidden" name="sub_activity_id1" id="sub_activity_id1" class="sub_activity_id">
                    <label for="sub_activity1">กิจกรรมที่จะดำเนินการ</label>
                    <input type="text" class="form-control sub_activity" id="sub_activity1" name="sub_activity1"
                        placeholder="กิจกรรมย่อย">
                </div>
                <div class="form-group col-md-2">
                    <label for="activity_month1">เดือนที่จะดำเนินการ</label>
                    <select id="activity_month1" class="form-control sub_month" name="activity_month1">
                        <option value="" selected>เลือกเดือน...</option>
                        <option value="1">มกราคม</option>
                        <option value="2">กุมภาพันธ์</option>
                        <option value="3">มีนาคม</option>
                        <option value="4">เมษายน</option>
                        <option value="5">พฤษภาคม</option>
                        <option value="6">มิถุนายน</option>
                        <option value="7">กรกฎาคม</option>
                        <option value="8">สิงหาคม</option>
                        <option value="9">กันยายน</option>
                        <option value="10">ตุลาคม</option>
                        <option value="11">พฤศจิกายน</option>
                        <option value="12">ธันวาคม</option>
                    </select>
                </div>
                <div class="form-group col-md-1" id="btn_del">
                    <label for="inputZip1">ลบ&nbsp;&nbsp;&nbsp;&nbsp;</label>
                    <button type="button" id="delAct1" class="btn btn-danger mb-1 btn-sm"
                        onclick="delActivity(this.id)"><i class="fas fa-cut"></i></button>
                </div>

                <div class="form-group col-md-6">
                    <input type="text" class="form-control sub_target" id="sub_activity_target1" name="sub_activity_target1"
                        placeholder="กลุ่มเป้าหมาย เช่น นักเรียนอายู 0-12 ปี">
                </div>
                <div class="form-group col-md-6">
                    <input type="text" class="form-control sub_purpose" id="sub_activity_purpose1" name="sub_activity_purpose1"
                        placeholder="วัตถุประสงค์ย่อย">
                </div>

            </div>

        </div>
    </div>

    <div class="form-group row">
        <label for="purpose" class="col-sm-2 col-form-label">วัตถุประสงค์</label>
        <div class="col-sm-10">
            <!-- <input type="text" class="form-control" id="purpose" name="purpose" placeholder=""> -->
            <textarea class="form-control" id="purpose" name="purpose" placeholder="" rows="6" cols="50"></textarea>
        </div>
    </div>


        <div class="form-group row">
            <label for="inputEmail3" class="col-sm-2 col-form-label">แหล่งงบประมาณ</label>
            <div class="col-sm-10">
                <!-- Default inline 1-->
                <button type="button" class="btn btn-outline-secondary btn-sm" id="budget_source">
                    <div class="custom-control custom-radio custom-control-inline">
                        <input type="radio" class="custom-control-input" id="budget1" name="budget" value="1">
                        <label class="custom-control-label" for="budget1">PP</label>
                    </div>
                </button>

                <!-- Default inline 2-->
                <button type="button" class="btn btn-outline-secondary btn-sm" id="budget_source">
                    <div class="custom-control custom-radio custom-control-inline">
                        <input type="radio" class="custom-control-input" id="budget2" name="budget" value="2"
                            onchange="showthis(this)">
                        <label class="custom-control-label" for="budget2">OP</label>
                    </div>
                </button>

                <!-- Default inline 3-->
                <button type="button" class="btn btn-outline-secondary btn-sm" id="budget_source">
                    <div class="custom-control custom-radio custom-control-inline ">
                        <input type="radio" class="custom-control-input" id="budget3" name="budget" value="3">
                        <label class="custom-control-label" for="budget3">ค่าเสือม</label>
                    </div>
                </button>
                <!-- Default inline 3-->
                <button type="button" class="btn btn-outline-secondary btn-sm" id="budget_source">
                    <div class="custom-control custom-radio custom-control-inline">
                        <input type="radio" class="custom-control-input" id="budget4" name="budget" value="4">
                        <label class="custom-control-label" for="budget4">เงินบำรุง</label>
                    </div>
                </button>
                <button type="button" class="btn btn-outline-secondary btn-sm" id="budget_source">
                    <div class="custom-control custom-radio custom-control-inline">
                        <input type="radio" class="custom-control-input" id="budget5" name="budget" value="5">
                        <label class="custom-control-label" for="budget5">งบกระทรวงฯ</label>
                    </div>
                </button>
                <button type="button" class="btn btn-outline-secondary btn-sm" id="budget_source">
                    <div class="custom-control custom-radio custom-control-inline">
                        <input type="radio" class="custom-control-input" id="budget6" name="budget" value="6">
                        <label class="custom-control-label" for="budget6">งบพัฒนาจังหวัด</label>
                    </div>
                </button>
                <button type="button" class="btn btn-outline-secondary btn-sm" id="budget_source">
                    <div class="custom-control custom-radio custom-control-inline">
                        <input type="radio" class="custom-control-input" id="budget7" name="budget" value="7">
                        <label class="custom-control-label" for="budget7">งบกองทุนอื่น</label>
                    </div>
                </button>




            </div>
        </div>


        <div class="form-group row">
            <label for="budget_request" class="col-sm-2 col-form-label">งบประมาณที่ขอไว้</label>
            <div class="col-sm-10">
                <!-- <input type='money' min="1" max="99999999999" size="11" class="form-control" id="budget_request" name="budget_request"
             pattern="^\$\d{1,3}(,\d{3})*(\.\d+)?$" value="" data-type="currency" placeholder="$1,000,000.00" onkeyup="reformatText(this)">

         -->

                <input type="number" min="0" step="0.01" data-number-to-fixed="2" data-number-stepfactor="100"
                    class="form-control currency" oninput="checkNumber(this)" id="budget_request" name="budget_request"
                    placeholder="1,000,000.00" />

            </div>
        </div>
        <div class="form-group row">
            <label for="owner" class="col-sm-2 col-form-label">ผู้รับผิดชอบโครงการ</label>
            <div class="col-sm-10">
                <input type="text" class="form-control" id="owner" name="owner" placeholder="">
            </div>
        </div>

        <div class="form-group row">
            <div class="col-sm-10">
                <button type="button" id="save_plan_basic" class="btn btn-primary"
                    onclick="add_planBasic()">บันทึก</button>
            </div>
        </div>

</form>
