<div class="card" id="step2">
    <div class="card-header">
        <div class="stepwizard-step">
            <button id="first_step" type="button" class="btn btn-success btn-circle">2</button>
            เบิกจ่ายค่าใช้จ่ายตามเงื่อนไข
        </div>
    </div>
    <div class="card-body">
        <form>
        <div class="form-group row">
        <label for="select_sub_plan" class="col-sm-2 col-form-label">กิจกรรมที่จะตั้งค่าใช้จ่าย</label>
        <div class="col-sm-10">
            <select class="custom-select" id="select_sub_plan" name="select_sub_plan" >
                <option value="" selected>Choose...</option>
                <!-- <option value="1">One</option>
                      <option value="2">Two</option>
                      <option value="3">Three</option> -->
            </select>
        </div>
    </div>
            <?php
if (isset($_GET['plan_id'])) {
    ?>
                <input type="hidden" name="addcost_planid" id="addcost_planid" value=" <?php echo $_GET['plan_id'] ?>">
                <input type="hidden" name="plan_budget" id="plan_budget" value=" <?php echo $_GET['budget'] ?>">
            <?php

}
?>
            <table class="table table-bordered table-hover" id="cost">
                <thead>
                    <tr>
                        <th scope="col" colspan="5">หลักเกณฑ์การเบิกจ่าย

                            <div class="container">
                                <div class="row">
                                    <div class="col-lg-3">

                                        <div class="card">
                                            ค่าใช้จ่ายหลัก
                                            <div class="card-body">
                                                <div class="custom-control custom-checkbox btn btn-outline-secondary"
                                                    id="checkCost">
                                                    <input type="checkbox" class="custom-control-input"
                                                        id="checkbox_cost1" name="checkbox_cost1" value="1">
                                                    <label class="custom-control-label"
                                                        for="checkbox_cost1">เบี้ยเลี้ยง</label>
                                                </div>
                                                <div class="custom-control custom-checkbox btn btn-outline-secondary"
                                                    id="checkCost">
                                                    <input type="checkbox" class="custom-control-input"
                                                        id="checkbox_cost8" name="checkbox_cost8" value="8">
                                                    <label class="custom-control-label"
                                                        for="checkbox_cost8">วัสดุฝึกอบรม </label>
                                                </div>
                                            </div>
                                        </div>

                                    </div>

                                    <div class="col-lg-3">
                                        <div class="card">
                                            ค่าใช้จ่ายฝึกอบรม
                                            <div class="card-body">
                                                <!-- <h5 class="card-title">ค่าใช้จ่ายฝึกอบรม</h5> -->
                                                <div class="custom-control custom-checkbox btn btn-outline-secondary"
                                                    id="checkCost">
                                                    <input type="checkbox" class="custom-control-input"
                                                        id="checkbox_cost3" name="checkbox_cost3" value="3">
                                                    <label class="custom-control-label"
                                                        for="checkbox_cost3">ค่าอาหารครบมือ</label>
                                                </div>
                                                <div class="custom-control custom-checkbox btn btn-outline-secondary"
                                                    id="checkCost">
                                                    <input type="checkbox" class="custom-control-input"
                                                        id="checkbox_cost4" name="checkbox_cost4" value="4">
                                                    <label class="custom-control-label"
                                                        for="checkbox_cost4">ค่าอาหารไม่ครบมือ</label>
                                                </div>
                                                <div class="custom-control custom-checkbox btn btn-outline-secondary"
                                                    id="checkCost">
                                                    <input type="checkbox" class="custom-control-input"
                                                        id="checkbox_cost5" name="checkbox_cost5" value="5">
                                                    <label class="custom-control-label"
                                                        for="checkbox_cost5">อาหารว่างและเครื่องดืม</label>
                                                </div>


                                                <div class="custom-control custom-checkbox btn btn-outline-secondary"
                                                    id="checkCost">
                                                    <input type="checkbox" class="custom-control-input"
                                                        id="checkbox_cost2" name="checkbox_cost2" value="2">
                                                    <label class="custom-control-label"
                                                        for="checkbox_cost2">วิทยากร</label>
                                                </div>



                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-3">
                                        <div class="card">
                                            ค่าจ้างเหมาบริการ
                                            <div class="card-body">
                                                <div class="custom-control custom-checkbox btn btn-outline-secondary"
                                                    id="checkCost">
                                                    <input type="checkbox" class="custom-control-input"
                                                        id="checkbox_cost6" name="checkbox_cost6" value="6">
                                                    <label class="custom-control-label"
                                                        for="checkbox_cost6">จ้างเหมารถบัส</label>
                                                </div>
                                                <div class="custom-control custom-checkbox btn btn-outline-secondary"
                                                    id="checkCost">
                                                    <input type="checkbox" class="custom-control-input"
                                                        id="checkbox_cost7" name="checkbox_cost7" value="7">
                                                    <label class="custom-control-label"
                                                        for="checkbox_cost7">จ้างเหมารถตู้</label>
                                                </div>
                                                <div class="custom-control custom-checkbox btn btn-outline-secondary"
                                                    id="checkCost">
                                                    <input type="checkbox" class="custom-control-input"
                                                        id="checkbox_cost11" name="checkbox_cost11" value="11">
                                                    <label class="custom-control-label"
                                                        for="checkbox_cost11">จ้างเหมาทำป้าย</label>
                                                </div>
                                                <div class="custom-control custom-checkbox btn btn-outline-secondary"
                                                    id="checkCost">
                                                    <input type="checkbox" class="custom-control-input"
                                                        id="checkbox_cost12" name="checkbox_cost11" value="12">
                                                    <label class="custom-control-label"
                                                        for="checkbox_cost12">จ้างเหมาถ่ายเอกสาร</label>
                                                </div>



                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-lg-3">
                                        <div class="card">
                                            ค่าใช้สอยอื่นๆ
                                            <div class="card-body">
                                                <div class="custom-control custom-checkbox btn btn-outline-secondary"
                                                    id="checkCost">
                                                    <input type="checkbox" class="custom-control-input"
                                                        id="checkbox_cost9" cost9 name="checkbox_cost9" value="9">
                                                    <label class="custom-control-label"
                                                        for="checkbox_cost9">ค่าโดยสารเครื่องบิน
                                                    </label>
                                                </div>
                                                <div class="custom-control custom-checkbox btn btn-outline-secondary"
                                                    id="checkCost">
                                                    <input type="checkbox" class="custom-control-input"
                                                        id="checkbox_cost10" name="checkbox_cost10" value="10">
                                                    <label class="custom-control-label"
                                                        for="checkbox_cost10">เงินรางวัลในโครงการ
                                                    </label>
                                                </div>
                                                <div class="custom-control custom-checkbox btn btn-outline-secondary"
                                                    id="checkCost">
                                                    <input type="checkbox" class="custom-control-input"
                                                        id="checkbox_cost13" name="checkbox_cost13" value="13">
                                                    <label class="custom-control-label"
                                                        for="checkbox_cost13">ค่าใช้จ่ายอื่น ๆ
                                                    </label>
                                                </div>


                                            </div>
                                        </div>

                                    </div>

                                </div>
                            </div>

                            <!-- Default checked -->

                        </th>
                    </tr>
                    <tr>
                        <th scope="col">ลำดับ</th>
                        <th scope="col">รายการ</th>
                        <th scope="col">เงื่อนไข</th>
                        <th scope="col">อัตราค่าใช้จ่ายตามมาตรการประหยัด</th>

                    </tr>
                </thead>
                <tbody id="cost_tbody">
                    <tr id="cost1" style="display:none;">
                        <td>1</td>
                        <td>เบี้ยงเลี้ยง</td>
                        <td>
                            <div class="custom-control custom-radio" id="cost_type1">
                                <input type="radio" class="custom-control-input" id="cost1_criteria1"
                                    name="cost1_criteria" value="1">
                                <label class="custom-control-label"
                                    for="cost1_criteria1">ระดับต่ำกว่าเชี่ยวชาญ(C8)</label>
                            </div>
                            <div class="custom-control custom-radio" id="cost_type2">
                                <input type="radio" class="custom-control-input" id="cost1_criteria2"
                                    name="cost1_criteria" value="2">
                                <label class="custom-control-label" for="cost1_criteria2">ระดับเชี่ยวชาญ(C9)</label>
                            </div>
                        </td>

                        <td id="td1">
                            <div class="custom-control custom-radio">
                                <input type="radio" class="custom-control-input" id="savecost1_criteria1"
                                    name="cost1save_criteria" data-rate_type="1" value="240">
                                <label class="custom-control-label" for="cost1save_criteria1">240
                                    บาทต่อคนต่อวัน</label>
                            </div>

                            <div class="custom-control custom-radio">
                                <input type="radio" class="custom-control-input" id="savecost1_criteria2"
                                    name="cost1save_criteria" data-rate_type="2" value="270">
                                <label class="custom-control-label" for="cost1save_criteria2">270
                                    บาทต่อคนต่อวัน</label>
                            </div>

                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <div class="input-group-text">เป้าหมาย(คน)</div>
                                </div>
                                <input type="text" class="form-control form-control-sm" name="target1" id="target1"
                                    placeholder="คน" oninput="checkNumber(this)">
                                <div class="input-group-prepend">
                                    <div class="input-group-text">จำนวนวัน</div>
                                </div>
                                <input type="text" class="form-control form-control-sm" name="days1" id="days1"
                                    placeholder="วัน" oninput="checkNumber(this)">
                                <div class="input-group-prepend">
                                    <div class="input-group-text">รวมเงิน</div>
                                </div>
                                <input type="text" class="form-control form-control-sm" name="price1" id="price1"
                                    placeholder="บาท" readonly>
                            </div>






                        </td>

                    </tr>


                    <tr id="cost2">
                        <th scope="row">2</th>
                        <td>วิทยากร</td>
                        <td>
                            <div class="custom-control custom-radio">
                                <input type="radio" class="custom-control-input" id="lecturer_criteria1"
                                    name="cost2_criteria" value="1" >
                                <label class="custom-control-label" for="lecturer_criteria1">อบรมประเภท ข.</label>
                            </div>
                            <div class="custom-control custom-radio">
                                <input type="radio" class="custom-control-input" id="lecturer_criteria2"
                                    name="cost2_criteria" value="2">
                                <label class="custom-control-label" for="lecturer_criteria2">บรรยายเบิก ชม.ละ 1
                                    คน</label>
                            </div>
                            <div class="custom-control custom-radio">
                                <input type="radio" class="custom-control-input" id="lecturer_criteria3"
                                    name="cost2_criteria" value="3">
                                <label class="custom-control-label" for="lecturer_criteria3">อภิปรายไม่เกน 5
                                    คน</label>
                            </div>
                            <div class="custom-control custom-radio">
                                <input type="radio" class="custom-control-input" id="lecturer_criteria4"
                                    name="cost2_criteria" value="4">
                                <label class="custom-control-label" for="lecturer_criteria4">แบ่งกลุ่ม ๆ ล่ะไม่เกิน
                                    2
                                    คน</label>
                            </div>


                        </td>
                        <td id="td2">
                            <div class="custom-control custom-radio">
                                <input type="radio" class="custom-control-input" id="cost2_criteria1"
                                    name="cost2save_criteria" data-rate_type="1" value="600" checked>
                                <label class="custom-control-label" for="cost2_criteria1">ชม.ละไม่เกิน 600
                                    บาทต่อคน</label>
                            </div>
                            <!-- <div class="input-group">
                                    <div class="input-group-prepend">
                                        <div class="input-group-text">จำนวนเงิน</div>
                                    </div>
                                    <input type="text" class="form-control check-numeric" name="price2" id="price2"
                                        placeholder="บาท">
                                </div> -->

                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <div class="input-group-text">เป้าหมาย(คน)</div>
                                </div>
                                <input type="text" class="form-control form-control-sm" name="target2" id="target2"
                                    placeholder="คน" oninput="checkNumber(this)">
                                <div class="input-group-prepend">
                                    <div class="input-group-text">จำนวนวัน</div>
                                </div>
                                <input type="text" class="form-control form-control-sm" name="days2" id="days2"
                                    placeholder="วัน" oninput="checkNumber(this)">
                                <div class="input-group-prepend">
                                    <div class="input-group-text">รวมเงิน</div>
                                </div>
                                <input type="text" class="form-control form-control-sm" name="price2" id="price2"
                                    placeholder="บาท" readonly>
                            </div>

                        </td>

                    </tr>

                    <tr id="cost3">
                        <th scope="row">3</th>
                        <td>ค่าอาหารครบมื้อ<br>
                            (ฝึกอบรมประเภท ข และบุคคลภายนอก)</td>
                        <td>
                            <div class="custom-control custom-radio">
                                <input type="radio" class="custom-control-input" id="costfood_criteria1"
                                    name="cost3_criteria" value="1">
                                <label class="custom-control-label" for="costfood_criteria1">ในสถานที่ราชการ</label>
                            </div>
                            <div class="custom-control custom-radio">
                                <input type="radio" class="custom-control-input" id="costfood_criteria2"
                                    name="cost3_criteria" value="2">
                                <label class="custom-control-label" for="costfood_criteria2">เอกชน</label>
                            </div>

                        </td>
                        <td>
                            <div class="custom-control custom-radio">
                                <input type="radio" class="custom-control-input" id="costfoodsave_criteria1"
                                    name="cost3save_criteria" data-rate_type="1" value="300">
                                <label class="custom-control-label" for="costfoodsave_criteria1">ตามเรียกเก็บจริงไม่เกิน
                                    300
                                    บ/คน</label>
                            </div>

                            <div class="custom-control custom-radio">
                                <input type="radio" class="custom-control-input" id="costfoodsave_criteria2"
                                    name="cost3save_criteria" data-rate_type="2" value="500">
                                <label class="custom-control-label" for="costfoodsave_criteria2">ตามเรียกเก็บจริงไม่เกิน
                                    500
                                    บ/คน</label>
                            </div>
                            <!-- <div class="input-group ">
                                    <div class="input-group-prepend">
                                        <div class="input-group-text">จำนวนเงิน</div>
                                    </div>
                                    <input type="text" class="form-control check-numeric" name="price3" id="price3"
                                        placeholder="บาท">
                                </div> -->
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <div class="input-group-text">เป้าหมาย(คน)</div>
                                </div>
                                <input type="text" class="form-control form-control-sm" name="target3" id="target3"
                                    placeholder="คน" oninput="checkNumber(this)">
                                <div class="input-group-prepend">
                                    <div class="input-group-text">จำนวนวัน</div>
                                </div>
                                <input type="text" class="form-control form-control-sm" name="days3" id="days3"
                                    placeholder="วัน" oninput="checkNumber(this)">
                                <div class="input-group-prepend">
                                    <div class="input-group-text">รวมเงิน</div>
                                </div>
                                <input type="text" class="form-control form-control-sm" name="price3" id="price3"
                                    placeholder="บาท" readonly>
                            </div>

                        </td>

                    </tr>

                    <tr id="cost4">
                        <th scope="row">4</th>
                        <td>ค่าอาหารไม่ครบมื้อ<br>
                            (ฝึกอบรมประเภท ข. และบุคคลภายนอก)
                        </td>
                        <td>
                            <div class="custom-control custom-radio">
                                <input type="radio" class="custom-control-input" id="cost4_criteria1"
                                    name="cost4_criteria" value="1">
                                <label class="custom-control-label" for="cost4_criteria1">ในสถานที่ราชการ</label>
                            </div>
                            <div class="custom-control custom-radio">
                                <input type="radio" class="custom-control-input" id="cost4_criteria2"
                                    name="cost4_criteria" value="2">
                                <label class="custom-control-label" for="cost4_criteria2">เอกชน</label>
                            </div>

                        </td>
                        <td>
                            <div class="custom-control custom-radio">
                                <input type="radio" class="custom-control-input" id="cost4save_criteria1"
                                    name="cost4save_criteria" data-rate_type="1" value="60">
                                <label class="custom-control-label" for="cost4save_criteria1">ไม่เกิน 60
                                    บาท/มื้อ/คน</label>
                            </div>

                            <div class="custom-control custom-radio">
                                <input type="radio" class="custom-control-input" id="cost4save_criteria2"
                                    name="cost4save_criteria" data-rate_type="2" value="300">
                                <label class="custom-control-label" for="cost4save_criteria2">ตามเรียกเก็บจริงไม่เกิน
                                    300
                                    บ/คน</label>
                            </div>
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <div class="input-group-text">เป้าหมาย(คน)</div>
                                </div>
                                <input type="text" class="form-control form-control-sm" name="target4" id="target4"
                                    placeholder="คน" oninput="checkNumber(this)">
                                <div class="input-group-prepend">
                                    <div class="input-group-text">จำนวนมื้อ</div>
                                </div>
                                <input type="text" class="form-control form-control-sm" name="days4" id="days4"
                                    placeholder="มื้อ" oninput="checkNumber(this)">
                                <div class="input-group-prepend">
                                    <div class="input-group-text">รวมเงิน</div>
                                </div>
                                <input type="text" class="form-control form-control-sm" name="price4" id="price4"
                                    placeholder="บาท" readonly>
                            </div>
                        </td>

                    </tr>
                    <tr id="cost5">
                        <th scope="row">5</th>
                        <td>อาหารว่างและเครื่องดื่ม (เช้า-บ่าย)

                        </td>
                        <td>
                            <div class="custom-control custom-radio">
                                <input type="radio" class="custom-control-input" id="cost5_criteria1"
                                    name="cost5_criteria" value="1">
                                <label class="custom-control-label"
                                    for="cost5_criteria1">ในสถานที่ราชการ/หน่วยงานรัฐ</label>
                            </div>
                            <div class="custom-control custom-radio">
                                <input type="radio" class="custom-control-input" id="cost5_criteria2"
                                    name="cost5_criteria" value="2">
                                <label class="custom-control-label" for="cost5_criteria2">เอกชน</label>
                            </div>

                        </td>
                        <td>
                            <div class="custom-control custom-radio">
                                <input type="radio" class="custom-control-input" id="cost5save_criteria1"
                                    name="cost5save_criteria" data-rate_type="1" value="25">
                                <label class="custom-control-label" for="cost5save_criteria1">ไม่เกิน 25
                                    บาท/มื้อ/คน</label>
                            </div>

                            <div class="custom-control custom-radio">
                                <input type="radio" class="custom-control-input" id="cost5save_criteria2"
                                    name="cost5save_criteria" data-rate_type="2" value="50">
                                <label class="custom-control-label" for="cost5save_criteria2">ไม่เกิน 50
                                    บาท/มื้อ/คน</label>
                            </div>

                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <div class="input-group-text">เป้าหมาย(คน)</div>
                                </div>
                                <input type="text" class="form-control form-control-sm" name="target5" id="target5"
                                    placeholder="คน" oninput="checkNumber(this)">
                                <div class="input-group-prepend">
                                    <div class="input-group-text">จำนวนมื้อ</div>
                                </div>
                                <input type="text" class="form-control form-control-sm" name="days5" id="days5"
                                    placeholder="มื้อ" oninput="checkNumber(this)">
                                <div class="input-group-prepend">
                                    <div class="input-group-text">รวมเงิน</div>
                                </div>
                                <input type="text" class="form-control form-control-sm" name="price5" id="price5"
                                    placeholder="บาท" readonly>
                            </div>


                            <!-- <div class="input-group">
                                <div class="input-group-prepend">
                                    <div class="input-group-text">จำนวนเงิน</div>
                                </div>
                                <input type="text" class="form-control check-numeric" name="price5" id="price5"
                                    placeholder="บาท" oninput="checkNumber(this)">
                            </div> -->
                        </td>

                    </tr>

                    <tr id="cost6">
                        <th scope="row">6</th>
                        <td>
                            จ้างเหมารถบัส
                        </td>
                        <td>


                            <div class="custom-control custom-radio" id="cost_type6">
                                <input type="radio" class="custom-control-input" id="cost6_criteria1"
                                    name="cost6_criteria" value="1">
                                <label class="custom-control-label" for="cost6_criteria1">ทั่วไป</label>
                            </div>
                            <div class="custom-control custom-radio">
                                <input type="radio" class="custom-control-input" id="cost6_criteria2"
                                    name="cost6_criteria" value="2">
                                <label class="custom-control-label" for="cost6_criteria2">ระยะทางไกลขึ้นที่สูง</label>
                            </div>

                        </td>
                        <td>
                            <div class="custom-control custom-radio">
                                <input type="radio" class="custom-control-input" id="cost6save_criteria1"
                                    name="cost6save_criteria" data-rate_type="1" value="15000">
                                <label class="custom-control-label" for="cost6save_criteria1">ไม่เกิน 15000
                                    บาท/คัน</label>
                            </div>

                            <div class="custom-control custom-radio">
                                <input type="radio" class="custom-control-input" id="cost6save_criteria2"
                                    name="cost6save_criteria" data-rate_type="2" value="17000">
                                <label class="custom-control-label" for="cost6save_criteria2">ไม่เกิน 17000
                                    บาท/คัน</label>
                            </div>
                            <div class="input-group mb-2">
                                <div class="input-group-prepend">
                                    <div class="input-group-text">จำนวนเงิน</div>
                                </div>
                                <input type="text" class="form-control check-numeric" name="price6" id="price6"
                                    placeholder="บาท" oninput="checkNumber(this)">
                            </div>
                        </td>

                    </tr>

                    <tr id="cost7">
                        <th scope="row">7</th>
                        <td>
                            จ้างเหมารถตู้
                        </td>
                        <td>
                            <div class="custom-control custom-radio">
                                <input type="radio" class="custom-control-input" id="cost7_criteria1"
                                    name="cost7_criteria" value="1">
                                <label class="custom-control-label" for="cost7_criteria1">ในจังหวัด</label>
                            </div>
                            <div class="custom-control custom-radio">
                                <input type="radio" class="custom-control-input" id="cost7_criteria2"
                                    name="cost7_criteria" value="2">
                                <label class="custom-control-label" for="cost7_criteria2">นอกจังหวัด (ไกล)</label>
                            </div>

                        </td>
                        <td>
                            <div class="custom-control custom-radio">
                                <input type="radio" class="custom-control-input" id="cost7save_criteria1"
                                    name="cost7save_criteria" data-rate_type="1" value="2500">
                                <label class="custom-control-label" for="cost7save_criteria1">ตั้งแต่ 2000 บาท
                                    ไม่เกิน 2500
                                    บาท/คัน
                                    รวมน้ำมัน (ขึ้นกับระยะทาง)
                                </label>
                            </div>

                            <div class="custom-control custom-radio">
                                <input type="radio" class="custom-control-input" id="cost7save_criteria2"
                                    name="cost7save_criteria" data-rate_type="2" value="1800">
                                <label class="custom-control-label" for="cost7save_criteria2">ตั้งแต่ 1800 บาท/คัน
                                    ไม่รวมน้ำมัน
                                    (น้ำมันตามจ่ายจริง) ค่าน้ำมันตั้งไว้ที่งบกลาง
                                </label>
                            </div>
                            <div class="input-group mb-2">
                                <div class="input-group-prepend">
                                    <div class="input-group-text">จำนวนเงิน</div>
                                </div>
                                <input type="text" class="form-control check-numeric" name="price7" id="price7"
                                    placeholder="บาท" oninput="checkNumber(this)">
                            </div>
                        </td>

                    </tr>
                    <tr id="cost8">
                        <th scope="row">8</th>
                        <td>
                            วัสดุฝึกอบรม
                        </td>
                        <td>
                            <div class="custom-control custom-radio">
                                <input type="radio" class="custom-control-input" id="cost8_criteria1"
                                    name="cost8_criteria" value="1" checked>
                                <label class="custom-control-label" for="cost8_criteria1">ไม่ระบุเงื่อนไข</label>
                            </div>

                        </td>
                        <td>
                            <div class="custom-control custom-radio">
                                <input type="radio" class="custom-control-input" id="cost8save_criteria2"
                                    name="cost8save_criteria" data-rate_type="1" value="20">
                                <label class="custom-control-label" for="cost8save_criteria2">ตามความจำเป็น
                                    (แต่ไม่เกิน 20 บาท/คน)
                                </label>
                            </div>

                            <div class="input-group mb-2">
                                <div class="input-group-prepend">
                                    <div class="input-group-text">จำนวนเงิน</div>
                                </div>
                                <input type="text" class="form-control check-numeric" name="price8" id="price8"
                                    placeholder="บาท" oninput="checkNumber(this)">
                            </div>
                        </td>

                    </tr>


                    <tr id="cost9">
                        <th scope="row">9</th>
                        <td>
                            ค่าโดยสารเครื่องบิน
                        </td>
                        <td>
                            <div class="custom-control custom-radio">
                                <input type="radio" class="custom-control-input" id="cost9_criteria1"
                                    name="cost9_criteria" value="1" checked>
                                <label class="custom-control-label" for="cost9_criteria1">เฉพาะผู้บริหารและ
                                    หน.กลุ่มงาน และบุคคลากรระดับชำนาญการขึ้นไปที่สามารถเบิกได้ตามสิทธิ
                                    (ช่วยประหยัดไม่จอผ่านบริษัท)</label>
                            </div>


                        </td>
                        <td>


                            <div class="custom-control custom-radio">
                                <input type="radio" class="custom-control-input" id="cost9save_criteria2"
                                    name="cost9save_criteria" data-rate_type="1" value="15000" checked>
                                <label class="custom-control-label" for="cost9save_criteria2"> บุคคลอื่น
                                    พิจารณาตามความจำเป็น เร่งด่วน(บอกเหตุราชการที่นั่งรถยนต์ไปไม่ทัน)
                                    กรณีพื้นที่ห่างไกล
                                    เช่น ภาคเหนือ ภาคใต้ หรือผู้บริหารมอบหมายไปแทน
                                </label>
                            </div>
                            <div class="input-group mb-2">
                                <div class="input-group-prepend">
                                    <div class="input-group-text">จำนวนเงิน</div>
                                </div>
                                <input type="text" class="form-control check-numeric" name="price9" id="price9"
                                    placeholder="บาท" oninput="checkNumber(this)">
                            </div>
                        </td>

                    </tr>
                    <tr id="cost10">
                        <th scope="row">10</th>
                        <td>
                            เงินรางวัลในโครงการ
                        </td>
                        <td>


                            <div class="custom-control custom-radio" id="cost_type1">
                                <input type="radio" class="custom-control-input" id="cost10_criteria1"
                                    name="cost10_criteria" value="1" checked>
                                <label class="custom-control-label" for="cost10_criteria1">ไม่ระบุเงื่อนไข</label>
                            </div>

                        </td>
                        <td>
                            <div class="custom-control custom-radio">
                                <input type="radio" class="custom-control-input" id="cost10save_criteria1"
                                    name="cost10save_criteria" data-rate_type="1" value="10000">
                                <label class="custom-control-label" for="cost10save_criteria1"> จ่ายให้บุคคล
                                    (ภายนอก)
                                    ไม่เกิน
                                    10000 บาท/โครงการ</label>
                            </div>

                            <div class="custom-control custom-radio">
                                <input type="radio" class="custom-control-input" id="cost10save_criteria2"
                                    name="cost10save_criteria" data-rate_type="2" value="30000">
                                <label class="custom-control-label" for="cost10save_criteria2">
                                    จ่ายให้หน่วยงาน/องค์กร
                                    ไม่เกิน
                                    30000 บาท/โครงการ</label>
                            </div>
                            <div class="input-group ">
                                <div class="input-group-prepend">
                                    <div class="input-group-text">จำนวนเงิน</div>
                                </div>
                                <input type="text" class="form-control check-numeric" name="price10" id="price10"
                                    placeholder="บาท" oninput="checkNumber(this)">
                            </div>
                        </td>

                    </tr>
                    <tr id="cost11">
                        <th scope="row">11</th>
                        <td>
                            จ้างเหมาทำป้าย
                        </td>
                        <td>
                            <div class="custom-control custom-radio">
                                <input type="radio" class="custom-control-input" id="cost11_criteria1"
                                    name="cost11_criteria" value="1" checked>
                                <label class="custom-control-label" for="cost11_criteria1">ตามราคาจริง</label>
                            </div>


                        </td>
                        <td>
                            <div class="custom-control custom-radio">
                                <input type="radio" class="custom-control-input" id="cost11save_criteria1"
                                    name="cost11save_criteria" data-rate_type="1" value="2500" checked>
                                <label class="custom-control-label" for="cost11save_criteria1">ตามราคาจริง
                                </label>
                            </div>


                            <div class="input-group mb-2">
                                <div class="input-group-prepend">
                                    <div class="input-group-text">จำนวนเงิน</div>
                                </div>
                                <input type="text" class="form-control check-numeric" name="price11" id="price11"
                                    placeholder="บาท" oninput="checkNumber(this)">
                            </div>
                        </td>

                    </tr>
                    <tr id="cost12">
                        <th scope="row">12</th>
                        <td>
                            จ้างเหมาถ่ายเอกสาร
                        </td>
                        <td>
                            <div class="custom-control custom-radio">
                                <input type="radio" class="custom-control-input" id="cost12_criteria1"
                                    name="cost12_criteria" value="1" checked>
                                <label class="custom-control-label" for="cost12_criteria1">ตามราคาจริง</label>
                            </div>


                        </td>
                        <td>
                            <div class="custom-control custom-radio">
                                <input type="radio" class="custom-control-input" id="cost12save_criteria1"
                                    name="cost12save_criteria" data-rate_type="1" value="2500" checked>
                                <label class="custom-control-label" for="cost12save_criteria1">ตามราคาจริง
                                </label>
                            </div>


                            <div class="input-group mb-2">
                                <div class="input-group-prepend">
                                    <div class="input-group-text">จำนวนเงิน</div>
                                </div>
                                <input type="text" class="form-control check-numeric" name="price12" id="price12"
                                    placeholder="บาท" oninput="checkNumber(this)">
                            </div>
                        </td>

                    </tr>
                    <tr id="cost13">
                        <th scope="row">13</th>
                        <td>
                            ค่าใช้จ่ายอื่น ๆ
                        </td>
                        <td>
                            <div class="custom-control custom-radio">
                                <input type="radio" class="custom-control-input" id="cost13_criteria1"
                                    name="cost13_criteria" value="1" checked>
                                <label class="custom-control-label" for="cost13_criteria1">ตามราคาจริง</label>
                            </div>


                        </td>
                        <td>
                            <div class="custom-control custom-radio">
                                <input type="radio" class="custom-control-input" id="cost13save_criteria1"
                                    name="cost13save_criteria" data-rate_type="1" value="2500" checked>
                                <label class="custom-control-label" for="cost13save_criteria1">ตามราคาจริง
                                </label>
                            </div>


                            <div class="input-group mb-2">
                                <div class="input-group-prepend">
                                    <div class="input-group-text">จำนวนเงิน</div>
                                </div>
                                <input type="text" class="form-control check-numeric" name="price13" id="price13"
                                    placeholder="บาท" oninput="checkNumber(this)">
                            </div>
                        </td>

                    </tr>
                </tbody>
            </table>
            <div class="form-group row">
                <div class="col-sm-10">
                    <button type="button" class="btn btn-primary" id="add_cost_plan"
                        onclick="saveCostPlan()">บันทึก</button>
                </div>
            </div>

        </form>
    </div>
</div>
<script>
   $(document).ready(function() {

    var len = $('script[src*="controller/add_plan.js"]').length;
    console.log(len);

        //if there are no scripts that match, the load it
        if (len === 0) {
            //loadScript('../controller/add_plan.js');
            $.getScript( "controller/add_plan.js", function( data, textStatus, jqxhr ) {
            console.log( data ); // Data returned
            //console.log( textStatus ); // Success
            //console.log( jqxhr.status ); // 200
            console.log( "Load was performed." );
            });
        }

    //     $('#pagination-container').pagination({
    // dataSource: [1, 2, 3, 4, 5, 6, 7, 8],
    // callback: function(data, pagination) {
    //     // template method of yourself
    //     var html = template(data);
    //     $('#data-container').html(html);
    // }
    // })
   });


    </script>
    <!-- <script scr="../vendor/pagination/pagination.min.js"></script> -->