<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Register</title>
    <!-- <link href="css/bootstrap.min.css" rel="stylesheet" media="screen"> -->

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.3/css/bootstrap.min.css"
        integrity="sha384-Zug+QiDoJOrZ5t4lssLdxGhVrurbmBWopoEl+M6BdEfwnCJZtKxi1KgxUyJq13dy" crossorigin="anonymous">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.11.2/css/all.min.css">
    
    <!-- <script type="text/javascript" src="js/jquery.min.js"></script> -->
    <script src="https://code.jquery.com/jquery-3.4.1.js"
        integrity="sha256-WpOohJOqMqqyKL9FccASB9O0KwACQJpFTUBLTYOVvVU=" crossorigin="anonymous"></script>
    <script type="text/javascript" src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.14.0/jquery.validate.min.js"></script>
    
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"
        integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous">
    </script>

    <link href="style.css" rel="stylesheet" type="text/css" media="screen">
    <script type="text/javascript" src="script.js"></script>
</head>
<body>

<div class="signin-form">

    <div class="container">


        <form class="form-signin" method="post" id="register-form">

            <h2 class="form-signin-heading">Sign Up</h2><hr />

            <div id="error">
            </div>

            <div class="form-group">
                <input type="text" class="form-control" placeholder="ชื่อ-นามสกุล" name="fullname" id="fullname" />
            </div>


            <div class="form-group">
                <input type="text" class="form-control" placeholder="Username" name="user_name" id="user_name" />
            </div>

            <div class="form-group">
                <input type="email" class="form-control" placeholder="Email address" name="user_email" id="user_email" />
                <span id="check-e"></span>
            </div>
            <div class="form-group">
                <label for="exampleFormControlSelect1">เลือกแผนก</label>
                <select class="form-control" id="dep" name="dep">
                    <option value=''>แผนก--</option>
                
                </select>
            </div>


            <!-- <div class="form-group">
                <input type="text" class="form-control" placeholder="User Level" name="user_level" id="user_level" />
                <span ></span>
            </div> -->
            <div class="form-check form-check-inline">
                <input class="form-check-input" type="radio" name="user_level" id="user_level1" value="1">
                <label class="form-check-label" for="inlineRadio1">ผู้ใช้งานทั่วไป</label>
            </div>
            <div class="form-check form-check-inline">
                <input class="form-check-input" type="radio" name="user_level" id="user_leve2" value="2">
                <label class="form-check-label" for="inlineRadio2">หัวหน้า/ผู้ตรวจสอบ</label>
            </div>
          


            <div class="form-group">
                <input type="password" class="form-control" placeholder="Password" name="password" id="password" />
            </div>

            <div class="form-group">
                <input type="password" class="form-control" placeholder="Retype Password" name="cpassword" id="cpassword" />
            </div>
            <hr />

            <div class="form-group">
                <button type="submit" class="btn btn-default" name="btn-save" id="btn-submit">
                <i class="fas fa-user-plus"></i> &nbsp; Create Account
                </button>
                <a href="../index.php"><button type="button" class="btn btn-default" name="btn-save" id="btn-home">
                <i class="fas fa-home"></i> &nbsp; Home
                </button></a>

            </div>

        </form>

    </div>

</div>

<script>
$('document').ready(function() {

    get_department();

});
function get_department(){
    $.getJSON('model/get_data_lookup.php', {limit: 0}, function (data) {
        $.each(data, function (key, value) {

            $('select#dep').append( '<option value="'+value.department_id+'">'+value.department_name+'</option>' );

          
        });
    });
}

</script>

<script src="js/bootstrap.min.js"></script>
</body>
</html>
