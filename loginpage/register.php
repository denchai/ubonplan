<?php
header('Content-Type: text/html; charset=utf-8');
require_once 'dbconfig.php';

if ($_POST) {
    $user_name = ($_POST['user_name']);
    $user_email = ($_POST['user_email']);
    $user_password = ($_POST['password']);
    $fullname = ($_POST['fullname']);
    $dep = ($_POST['dep']);
    $user_level = ($_POST['user_level']);
    $joining_date = date('Y-m-d H:i:s');

    //password_hash see : http://www.php.net/manual/en/function.password-hash.php
    // $password = password_hash($user_password, PASSWORD_BCRYPT, array('cost' => 11));
    $password = md5($user_password);

    try
    {
        $stmt = $db_con->prepare("SELECT * FROM users WHERE user_email=:email");
        $stmt->execute(array(":email" => $user_email));
        $count = $stmt->rowCount();

        

        if ($count == 0) {
            $stmt = $db_con->prepare("INSERT INTO users(username,passwrd,fullname,user_email,dep_id,user_level_id,register_date) 
            VALUES(:uname,  :pass,:fullname,:email,:dep, :ulevel,:jdate)");
            $stmt->bindParam(":uname", $user_name);
            $stmt->bindParam(":email", $user_email);
            $stmt->bindParam(":pass", $password);
            $stmt->bindParam(":fullname", $fullname);
            $stmt->bindParam(":dep", $dep);
            $stmt->bindParam(":ulevel", $user_level);
            $stmt->bindParam(":jdate", $joining_date);

            if ($stmt->execute()) {
                echo "registered";
            } else {
                echo "Query could not execute !";
            }

        } else {

            echo "1"; //  not available
        }

    } catch (PDOException $e) {
        echo $e->getMessage();
    }
}
