<link rel="stylesheet" href="css/add_plan.css">

<div class="container">
    <br>
    <!-- stepper -->
    <div class="stepwizard">
        <div class="stepwizard-row">
            <div class="stepwizard-step">
                <button id="first_step" type="button" class="btn btn-default btn-circle">1</button>
                <p>เพิ่มโครงการ</p>
            </div>
            <div class="stepwizard-step">
                <button id="second_step" type="button" class="btn btn-default btn-circle">2</button>
                <p>บันทึกเบิกจ่าย</p>
            </div>
            <div class="stepwizard-step">
                <button type="button" class="btn btn-default btn-circle">3</button>
                <p>แนบภาพSMS</p>
            </div>

            <div class="stepwizard-step">
                <button type="button" class="btn btn-default btn-circle" disabled="disabled">4</button>
                <p>รออนุมัติ</p>
            </div>
        </div>
    </div>
    <div class="card">
        <div class="card-header">
            <div class="stepwizard-step">
                <button id="first_step" type="button" class="btn btn-success btn-circle">1</button>
                ข้อมูลพื้นฐานโครงการ
            </div>
        </div>
        <div class="card-body">
            <?php
                include "pages_include/add_plan_basic.php";
            ?>
        </div>
    </div>
    
    <?php 
       include "pages_include/add_plan_cost.php";
       
       include "pages_include/add_plan_image.php";
    ?>
</div>

<script src="controller/add_plan.js"></script>