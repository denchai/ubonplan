<!-- <!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <title>Plans</title> -->
    <link rel="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.css">
    <link rel="https://cdn.datatables.net/1.10.20/css/dataTables.bootstrap4.min.css">



    <!-- <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css"> -->
    <script src="media/js/jquery.js" type="text/javascript"></script>
    <!-- <script src="media/js/jquery.dataTables.js" type="text/javascript"></script> -->

    <script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap4.min.js"></script>






    <style>
    .cb-dropdown-wrap {
        max-height: 80px;
        overflow-y: auto;
        border: 1px solid #888;
    }

    .cb-dropdown,
    .cb-dropdown li {
        margin: 0;
        padding: 0;
        list-style: none;
    }

    .cb-dropdown li label {
        display: block;
        position: relative;
        cursor: pointer;
    }

    .cb-dropdown li label>input {
        position: absolute;
        right: 0;
        top: 0;
        width: 16px;
    }

    .cb-dropdown li label>span {
        display: block;
        margin-left: 3px;
        margin-right: 20px;
        font-family: sans-serif;
        font-size: 0.8em;
        font-weight: normal;
        text-align: left;
    }
    </style>


<!-- </head>

<body> -->


    <div class="container">


        <div class="row">
            <div class="col-lg-12">

                <div class="card">
                    <div class="card-header">
                        ข้อมูลโครงการทั้งหมด
                    </div>

                    <div class="card-body">
                        <!-- <div class="form-check form-check-inline">
                            <div class="input-group-prepend">
                                <span class="input-group-text" id="basic-addon1">กรองข้อมูล</span>
                            </div>


                        </div>


                        <div class="form-check form-check-inline">

                            <input type="checkbox" class="form-check-input" id="materialInline1">
                            <label class="form-check-label" for="materialInline1">รออนุมัติ</label>
                        </div>


                        <div class="form-check form-check-inline">
                            <input type="checkbox" class="form-check-input" id="materialInline2">
                            <label class="form-check-label" for="materialInline2">อนุมัติแล้ว</label>
                        </div>


                        <div class="form-check form-check-inline">
                            <input type="checkbox" class="form-check-input" id="materialInline3">
                            <label class="form-check-label" for="materialInline3">ไม่อนุมัติ</label>
                        </div>

 -->


                        <table id="example" class="table table-striped table-bordered">
                            <thead>
                                <tr>
                                    <th>plan_id</th>
                                    <th>ชื่อโครงการ</th>
                                    <th>วันกรอก</th>
                                    <th>ผู้รับผิดชอบ.</th>
                                    <th>แหล่งงบฯ</th>
                                    <th>งบประมาณ</th>
                                    <th>สถานะ</th>
                                    <th>#</th>
                                </tr>
                            </thead>
                            <tfoot>
                                <tr>
                                    <th>plan_id</th>
                                    <th>ชื่อโครงการ</th>
                                    <th>วันกรอก</th>
                                    <th>ผู้รับผิดชอบ.</th>
                                    <th>แหล่งงบฯ</th>
                                    <th>งบประมาณ</th>
                                    <th>สถานะ</th>
                                    <th>#</th>
                                </tr>
                            </tfoot>
                        </table>
                        <div id="editPlanBasic" class="container">
                            <?php
include "pages_include/add_plan_basic.php";
?>
                        </div>
                        <div id="editPlanImage" class="container d-none">
                            <?php
include "pages_include/add_plan_image.php";
?>
                        </div>


                    </div>
                </div>
            </div>


        </div>



    </div>

    <script type="text/javascript">
    $(document).ready(function() {

        var sessionUserLevel = '<?php echo $_SESSION['user_level']; ?>';


        function cbDropdown(column) {
            return $('<ul>', {
                'class': 'cb-dropdown'
            }).appendTo($('<div>', {
                'class': 'cb-dropdown-wrap'
            }).appendTo(column));
        }

        $.extend(true, $.fn.dataTable.defaults, {
            "language": {
                "sProcessing": "กำลังดำเนินการ...",
                "sLengthMenu": "<form class='form-inline'>แสดง_MENU_ แถว</form>",
                "sZeroRecords": "ไม่พบข้อมูล",
                "sInfo": "แสดง _START_ ถึง _END_ จาก _TOTAL_ แถว",
                "sInfoEmpty": "แสดง 0 ถึง 0 จาก 0 แถว",
                "sInfoFiltered": "(กรองข้อมูล _MAX_ ทุกแถว)",

                "sInfoPostFix": "",
                "sSearch": "ค้นหา:",
                "sUrl": "",                
                "oPaginate": {
                    "sFirst": "เิริ่มต้น",
                    "sPrevious": "ก่อนหน้า",
                    "sNext": "ถัดไป",
                    "sLast": "สุดท้าย"
                }
            },

        });

        if(sessionUserLevel == 3){
            var defaultContent ="<div class='btn-group' role='group' aria-label='Basic example' >" +
                        "<button type='button' class='btn btn-success btn-sm' id='approve' data-status_id='2'>อนุมัติ</button>" +
                        "<button type='button' class='btn btn-danger btn-sm' id='not_approve' data-status_id='3'>ไม่อนุมัติ</button>" +
                        "<div class='dropdown'>" +
                        "<button class='btn btn-secondary btn-sm dropdown-toggle' type='button' id='dropdownMenuButton' data-toggle='dropdown' aria-haspopup='true' aria-expanded='false'>" +
                        "จัดการ</button>" +
                        "<div class='dropdown-menu' aria-labelledby='dropdownMenuButton'>" +
                        "<a id='show_detail' class='dropdown-item' href='#' >รายละเอียด</a>" +
                        "<a id='edit_basic' class='dropdown-item' href='#'>เพิ่ม/แก้ไขข้อมูลและกิจกรรม</a>" +
                        "<a id='add_cost' class='dropdown-item' href='#'>เพิ่ม/แก้ไขค่าใช้จ่าย</a>" +

                        "<a id='upload_image' class='dropdown-item' href='#'>อัพโหลดรูป</a>" +
                        "<a id='approveDoc' class='dropdown-item' href='#'>พิมพ์ใบขอนุมัติ</a>" +
                        "<a id='del_plan' class='dropdown-item' href='#'>ลบ</a>" +
                        "</div></div></div>";
        }else{
            var defaultContent ="<div class='btn-group' role='group' aria-label='Basic example' >" +
                        "<div class='dropdown'>" +
                        "<button class='btn btn-secondary btn-sm dropdown-toggle' type='button' id='dropdownMenuButton' data-toggle='dropdown' aria-haspopup='true' aria-expanded='false'>" +
                        "จัดการ</button>" +
                        "<div class='dropdown-menu' aria-labelledby='dropdownMenuButton'>" +
                        "<a id='show_detail' class='dropdown-item' href='#' >รายละเอียด</a>" +
                        "<a id='edit_basic' class='dropdown-item' href='#'>เพิ่ม/แก้ไขข้อมูลและกิจกรรม</a>" +
                        "<a id='add_cost' class='dropdown-item' href='#'>เพิ่ม/แก้ไขค่าใช้จ่าย</a>" +

                        "<a id='upload_image' class='dropdown-item' href='#'>อัพโหลดรูป</a>" +
                        "<a id='approveDoc' class='dropdown-item' href='#'>พิมพ์ใบขอนุมัติ</a>" +
                        "<a id='del_plan' class='dropdown-item' href='#'>ลบ</a>" +
                        "</div></div></div>";
        }

        var table = $('#example').DataTable({
            "ajax": "ajaxData/plans_data.php",
            "columns": [{
                    "data": "activity_plan_id"
                },
                {
                    "data": "plan_name"
                },
                {
                    "data": "date_input"
                },
                {
                    "data": "owner"
                },
                {
                    "data": "budget_name"
                },
                {
                    "data": "budget_request",
                    "render": $.fn.dataTable.render.number( ',', '.', 2, '' )                    

                },
                {
                    "data": "status_name",
                    "width": "12%"
                },

                // { "data": "user_id" },
                {
                    "data": null,
                    "defaultContent": defaultContent,

                    "targets": -1
                }
            ],
            
            "columnDefs": [{
                className: "my_class",
                "targets": [6]
            },
            // {
            //     "targets": [ 5 ],
            //     "visible": false
            // }
        ],
        "lengthMenu": [[5, 10, 15, -1], [5, 10, 15, 20]],

            //"dom": '<"top"if>rt<"bottom"p>',
            // dom: 'B<"clear">lfrtip',

            buttons: true,
            "buttons": [
                'copy', 'excel', 'pdf'
            ],

            responsive: true,
            //.every
            initComplete: function() {
                if(sessionUserLevel!= '3'){
                    $('button#approve').hide();
                    $('button#not_approve').hide();
                }
                this.api().columns('.my_class').every(function() {
                    var column = this;
                    var ddmenu = cbDropdown($(column.header()).empty())
                        .on('change', ':checkbox', function() {
                            var vals = $(':checked', ddmenu).map(function(index,
                                element) {
                                return $.fn.dataTable.util.escapeRegex($(
                                    element).val());
                            }).toArray().join('|');

                            column
                                .search(vals.length > 0 ? '^(' + vals + ')$' : '', true,
                                    false)
                                .draw();
                        });

                    column.data().unique().sort().each(function(d, j) {
                        var // wrapped
                            $label = $('<label>'),
                            $text = $('<span>', {
                                text: d
                            }),
                            $cb = $('<input>', {
                                type: 'checkbox',
                                value: d
                            });

                        $text.appendTo($label);
                        $cb.appendTo($label);

                        ddmenu.append($('<li>').append($label));
                    });
                });
            },





        });


        $('#example tbody').on('click', 'a#edit_basic', function() {
            var data = table.row($(this).parents('tr')).data();
            popupEditPlanBasic(data.activity_plan_id);
        });
        $('#example tbody').on('click', 'a#upload_image', function() {
            var data = table.row($(this).parents('tr')).data();
            popupEditPlanImage(data.activity_plan_id);
        });

        $('#example tbody').on('click', 'a#show_detail', function() {
            var data = table.row($(this).parents('tr')).data();
            // console.log(data.activity_plan_id);
            window.open('views/plan_printA4.php?plan_id=' + data.activity_plan_id+'&budget='+data.budget_request, '_blank');
        });
        
        $('#example tbody').on('click', 'a#approveDoc', function() {
            var data = table.row($(this).parents('tr')).data();
            // console.log(data.activity_plan_id);
            window.open('views/plan_approveA4.php?plan_id=' + data.activity_plan_id+'&budget='+data.budget_request, '_blank');
        });

        $('#example tbody').on('click', 'a#add_cost', function() {
            var data = table.row($(this).parents('tr')).data();
            // console.log(data.activity_plan_id);

            window.open('index.php?page=add_cost&plan_id=' + data.activity_plan_id+'&budget='+data.budget_request, '_seft');
        });

        $('#example tbody').on('click', 'a#del_plan', function() {
            var data = table.row($(this).parents('tr')).data();
            // console.log(data.activity_plan_id);
           // console.log( table.row($(this).parents('tr')).index());
            table.row( $(this).parents('tr') ).remove().draw();

            delPlan(data.activity_plan_id);
        });

        $('#example tbody').on('click', 'button#approve', function() {
            var data = table.row($(this).parents('tr')).data();
            var row = table.row($(this).parents('tr'));
            // console.log(data.activity_plan_id);
            var status_id= $(this).attr('data-status_id');
            var cellData = table.cell(row, 6).data();
            //alert(cellData);

            setApprove(data.activity_plan_id,status_id,function() {
                if(cellData == 'ไม่อนุมัติ'){
                    alert(cellData);
                    table.cell(row, 6).data('อนุมัติ').draw();
                }else{
                    table.cell(row, 6).data('ไม่อนุมัติ').draw();
                }
           });

        });

        $('#example tbody').on('click', 'button#not_approve', function() {
            var data = table.row($(this).parents('tr')).data();
            var row = table.row($(this).parents('tr'));
            // console.log(data.activity_plan_id);
            var status_id= $(this).attr('data-status_id');
            //var id = this.id;
            // setApprove(data.activity_plan_id,status_id);
            var cellData = table.cell(row, 6).data();
            setApprove(data.activity_plan_id,status_id,function() {
                if(cellData == 'ไม่อนุมัติ'){
                    //alert(cellData);
                    table.cell(row, 6).data('อนุมัติ').draw();
                }else{
                    table.cell(row, 6).data('ไม่อนุมัติ').draw();
                }
           });

        });

        if(sessionUserLevel!= '3'){
            $('button#approve').hide();
            $('button#not_approve').hide();
        }

        //override css for dataTables
        $('div#example_filter').addClass("form-inline");
        $('div#example_filter').css('float','right');


    });


    function setApprove(plan_id,status_id,callback) {
        //console.log(status_id);
        if(status_id == '2'){
            var textNotifiy = 'คุณต้องการจะอนุมุัติ..โครงการนี้หรือไม่';
        }else{
            var textNotifiy = 'คุณต้องการจะไม่..อนุมุัติโครงการนี้หรือไม่';
        }

        alertify.confirm('คำเตือน', textNotifiy, function() {

            $.getJSON('model/approve.php', {
                'status_id': status_id,
                'plan_id': plan_id
            }, function(data) {
                $.each(data, function(key, value) {
                    //console.log(value.result);
                    alertify.success('Ok Update Plan Id: ' + value.return_id);
                    callback();
                });
            });

        }, function() {
            alertify.error('Cancel')
        });
    }



    </script>
    <script src="controller/plan.js"></script>
    <!-- <script src="helper/helper.js"></script> -->
<!-- </body>

</html> -->